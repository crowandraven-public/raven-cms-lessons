<?php

return [
    'lessons' => [
        'index' => [
            'title'                     => 'All Lessons',
            'search' => [
                'title'                 => 'Lessons with Query :search',
                'label'                 => 'Search Keywords',
                'placeholder'           => 'Search Lessons',
            ],
            'add_new'                   => 'Add New',
            'table' => [
                'featured'              => 'Featured',
                'private'               => 'Private',
                'title'                 => 'Title',
                'updated'               => 'Updated',
                'delete'                => 'Delete Lesson',
                'delete_message'        => 'Type the word DESTROY below to delete this lesson.',
                'delete_confirm'        => 'Are you sure? Lesson deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add Lesson',
            'form' => [
                'title'                 => 'Title',
                'content'               => 'Content',
                'length'                => 'Length',
                'length_help'           => 'In seconds. Example: \'330\'',
                'config'                => 'Configuration',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'author'                => 'Author',
                'image'                 => 'Image',
                'file'                  => 'File',
                'video'                 => 'Video',
                'video_help'            => 'Absolute URL for the hosted video to show for the course intro. Example: https://player.vimeo.com/video/204807063',
                'quiz'                  => 'Quiz',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta',
                'attachments'           => 'Attachments',
                'resources'             => 'Related Resources',
                'add_resource'          => 'Add Resource',
                'is_featured'           => 'Is Featured',
                'is_private'            => 'Is Private',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Lesson',
            'form' => [
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'content'               => 'Content',
                'excerpt'               => 'Excerpt',
                'length'                => 'Length',
                'length_help'           => 'In seconds. Example: \'330\'',
                'config'                => 'Configuration',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'author'                => 'Author',
                'image'                 => 'Image',
                'file'                  => 'File',
                'video'                 => 'Video',
                'video_help'            => 'Absolute URL for the hosted video to show for the course intro. Example: https://player.vimeo.com/video/204807063',
                'quiz'                  => 'Quiz',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta',
                'attachments'           => 'Attachments',
                'resources'             => 'Related Resources',
                'add_resource'          => 'Add Resource',
                'is_featured'           => 'Is Featured',
                'is_private'            => 'Is Private',
            ],
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Lesson Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Lesson Error',
                    'message'           => 'Hmmm, there was a problem creating the lesson.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Lesson Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the lesson.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Lesson Deleted',
                    'message'           => 'The lesson was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that lesson.',
                ]
            ]
        ]
    ],
    'quizzes' => [
        'index' => [
            'title'                     => 'All Quizzes',
            'search' => [
                'title'                 => 'Quizzes with Query :search',
                'label'                 => 'Search Keywords',
                'placeholder'           => 'Search Quizzes',
            ],
            'add_new'                   => 'Add New',
            'table' => [
                'title'                 => 'Title',
                'updated'               => 'Updated',
                'delete'                => 'Delete Quiz',
                'delete_message'        => 'Type the word DESTROY below to delete this quiz.',
                'delete_confirm'        => 'Are you sure? Quiz deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add Quiz',
            'form' => [
                'title'                 => 'Title',
                'content'               => 'Content',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'locale'                => 'Locale',
                'config'                => 'Configuration',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta'
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Quiz',
            'form' => [
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'content'               => 'Content',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'locale'                => 'Locale',
                'config'                => 'Configuration',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta'
            ],
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Quiz Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Quiz Error',
                    'message'           => 'Hmmm, there was a problem creating the quiz.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Quiz Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the quiz.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Quiz Deleted',
                    'message'           => 'The quiz was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that quiz.',
                ]
            ]
        ]
    ],
    'series' => [
        'index' => [
            'title'                     => 'All Series',
            'search' => [
                'title'                 => 'Series with Query :search',
                'label'                 => 'Search Keywords',
                'placeholder'           => 'Search Series',
            ],
            'add_new'                   => 'Add New',
            'table' => [
                'featured'              => 'Featured',
                'private'               => 'Private',
                'title'                 => 'Title',
                'updated'               => 'Updated',
                'delete'                => 'Delete Series',
                'delete_message'        => 'Type the word DESTROY below to delete this series.',
                'delete_confirm'        => 'Are you sure? Series deletion is permanent and irreversible.',
            ],
        ],
        'create' => [
            'title'                     => 'Add Series',
            'form' => [
                'title'                 => 'Title',
                'content'               => 'Content',
                'recommended_for'       => 'Recommended For',
                'testimony_text'        => 'Testimony Text',
                'testimony_attrib'      => 'Testimony Attribution',
                'config'                => 'Configuration',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'author'                => 'Author',
                'image'                 => 'Image',
                'video'                 => 'Video',
                'video_help'            => 'Absolute URL for the hosted video to show for the course intro. Example: https://player.vimeo.com/video/204807063',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta',
                'attachments'           => 'Attachments',
                'is_featured'           => 'Is Featured',
                'is_private'            => 'Is Private',
                'lessons'               => 'Lessons',
                'add_lesson'            => 'Add Lesson',
            ],
        ],
        'edit' => [
            'title'                     => 'Edit Series',
            'form' => [
                'title'                 => 'Title',
                'slug'                  => 'Slug',
                'content'               => 'Content',
                'excerpt'               => 'Excerpt',
                'recommended_for'       => 'Recommended For',
                'testimony_text'        => 'Testimony Text',
                'testimony_attrib'      => 'Testimony Attribution',
                'config'                => 'Configuration',
                'tracks'                => 'Tracks',
                'topics'                => 'Topics',
                'author'                => 'Author',
                'image'                 => 'Image',
                'video'                 => 'Video',
                'video_help'            => 'Absolute URL for the hosted video to show for the course intro. Example: https://player.vimeo.com/video/204807063',
                'status'                => 'Status',
                'select_one'            => 'Select One',
                'meta'                  => 'Meta',
                'attachments'           => 'Attachments',
                'is_featured'           => 'Is Featured',
                'is_private'            => 'Is Private',
                'lessons'               => 'Lessons',
                'add_lesson'            => 'Add Lesson',
                'config'                => 'Configuration',
            ],
        ],
        'flash' => [
            'create' => [
                'success' => [
                    'title'             => 'Series Created',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Series Error',
                    'message'           => 'Hmmm, there was a problem creating the series.',
                ]
            ],
            'edit' => [
                'success' => [
                    'title'             => 'Series Updated',
                    'message'           => 'Woohoo, good work.',
                ],
                'error' => [
                    'title'             => 'Edit Error',
                    'message'           => 'Hmmm, there was a problem updating the series.',
                ]
            ],
            'delete' => [
                'success' => [
                    'title'             => 'Series Deleted',
                    'message'           => 'The series was successfully deleted.',
                ],
                'error' => [
                    'title'             => 'Deletion Error',
                    'message'           => 'Hmmm, there was an error deleting that series.',
                ]
            ]
        ]
    ]
];
