@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.lessons.edit.title'))

@section('scripts')
<script src="https://cdn.tiny.cloud/1/va0cczdbx8gc8utla2gv66pu2ip8nq2yanvgzp9sq0g1bvsj/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({
    selector:'textarea',
    height: 300,
    theme: 'silver',
    plugins: [
        'advlist autolink lists link image charmap hr anchor',
        'searchreplace wordcount visualblocks visualchars code',
        'media nonbreaking save table contextmenu directionality',
        'paste imagetools'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
    menubar: 'tools',
    image_advtab: true,
});</script>
@endsection

@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/lessons/'.$lesson->id, 'role' => 'form', 'method' => 'put']) !!}
@endsection

@section('content')
<h1>{{ __('raven::messages.lessons.edit.title') }}</h1>

<div class="row">
    <div class="col-md-12 margintop-xs marginbottom-xs">
        <a href="{{ env('APP_URL') }}/lessons/{{ localize($lesson->slug) }}">{{ env('APP_URL') }}/lessons/{{ localize($lesson->slug) }}</a>
    </div>
</div>

<!-- Title -->
<div class="row form-group">
    {!! Form::label('title', __('raven::messages.lessons.edit.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('title-'.$locale, localize($lesson->title, $locale), ['class' => 'form-control', 'required' => 'required']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('title-'.config('raven-cms.fallback_locale'), localize($lesson->title, config('raven-cms.fallback_locale')), ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    @endif
</div>

<!-- Slug -->
<div class="row form-group">
    {!! Form::label('slug', __('raven::messages.lessons.edit.form.slug'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('slug-'.$locale, localize($lesson->slug, $locale), ['class' => 'form-control', 'required' => 'required']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('slug-'.config('raven-cms.fallback_locale'), localize($lesson->slug, config('raven-cms.fallback_locale')), ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    @endif
</div>

<!-- Content -->
@if (config('raven-cms.show.lessons.content'))
    <div class="row form-group">
        {!! Form::label('content', __('raven::messages.lessons.edit.form.content'), ['class' => 'col-md-12 control-label']) !!}
        @if (is_array(config('raven-cms.locale')))
            <div class="panel panel-fieldset">
                <div class="panel-body">
                    @foreach (config('raven-cms.locale') as $locale)
                        <div class="col-md-12">
                            {!! Form::textarea('content-'.$locale, localize($lesson->content, $locale), ['class' => 'form-control','rows' => '5']) !!}
                            <p class="small help-block">{{ $locale }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="col-md-12">
                {!! Form::textarea('content-'.config('raven-cms.fallback_locale'), localize($lesson->content, config('raven-cms.fallback_locale')), ['class' => 'form-control','rows' => '5']) !!}
            </div>
        @endif
    </div>
@endif

@if (config('raven-cms.show.lessons.length') || config('raven-cms.show.lessons.config') || config('raven-cms.show.lessons.track_tags') || config('raven-cms.show.lessons.topic_tags') || config('raven-cms.show.lessons.author'))
    <div class="row">
        <div class="col-md-12">
            <h3>{{ __('raven::messages.lessons.edit.form.meta') }}</h3>
        </div>
    </div>
@endif

<!-- Excerpt -->
@if (config('raven-cms.show.lessons.excerpt'))
    <div class="row form-group">
        {!! Form::label('excerpt', __('raven::messages.lessons.edit.form.excerpt'), ['class' => 'col-md-12 control-label']) !!}
        @if (is_array(config('raven-cms.locale')))
            <div class="panel panel-fieldset">
                <div class="panel-body">
                    @foreach (config('raven-cms.locale') as $locale)
                        <div class="col-md-12">
                            {!! Form::text('excerpt-'.$locale, localize($lesson->excerpt, $locale), ['class' => 'form-control']) !!}
                            <p class="small help-block">{{ $locale }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="col-md-12">
                {!! Form::text('excerpt-'.config('raven-cms.fallback_locale'), localize($lesson->excerpt, config('raven-cms.fallback_locale')), ['class' => 'form-control']) !!}
            </div>
        @endif
    </div>
@endif

<!-- Length -->
@if (config('raven-cms.show.lessons.length'))
    <div class="row form-group">
        {!! Form::label('length', __('raven::messages.lessons.edit.form.length'), ['class' => 'col-md-12 control-label']) !!}
        <div class="col-md-12">
            {!! Form::text('length', $lesson->length, ['class' => 'form-control']) !!}
            <p class="small help-block">{{ __('raven::messages.lessons.edit.form.length_help') }}</p>
        </div>
    </div>
@endif

<!-- Config -->
@if (config('raven-cms.show.lessons.config'))
    <div class="row form-group">
        {!! Form::label('config', __('raven::messages.lessons.edit.form.config'), ['class' => 'col-md-12 control-label']) !!}
        <div class="col-md-12">
            {!! Form::text('config', $lesson->config, ['class' => 'form-control']) !!}
            <p class="small help-block">It currently accepts any valid JSON.</p>
        </div>
    </div>
@endif

<!-- Track Tags -->
@if (config('raven-cms.show.lessons.track_tags'))
    @if ($all_tracks->count() > 0)
        <div class="row form-group">
            {!! Form::label('track_tags', __('raven::messages.lessons.edit.form.tracks'), ['class' => 'col-md-12 control-label required']) !!}
            
            <div class="tags-container">
                @foreach ($all_tracks as $key => $value)
                    <span class="button-checkbox">
                        <button type="button" class="btn btn-tag" data-color="primary">{{ str_replace('"', '', $value) }}</button>
                        <input type="checkbox" class="hidden" name="track_tags[]" value="{{ $key }}" {{ (in_array($key, $lesson_tags->toArray()) ? 'checked' : '') }} />
                    </span>
                @endforeach
            </div>
        </div>
    @endif
@endif

<!-- Topic Tags -->
@if (config('raven-cms.show.lessons.topic_tags'))
    @if ($all_topics->count() > 0)
        <div class="row form-group">
            {!! Form::label('topic_tags', __('raven::messages.lessons.edit.form.topics'), ['class' => 'col-md-12 control-label']) !!}
            
            <div class="tags-container">
                @foreach ($all_topics as $key => $value)
                    <span class="button-checkbox">
                        <button type="button" class="btn btn-tag" data-color="primary">{{ str_replace('"', '', $value) }}</button>
                        <input type="checkbox" class="hidden" name="topic_tags[]" value="{{ $key }}" {{ (in_array($key, $lesson_tags->toArray()) ? 'checked' : '') }} />
                    </span>
                @endforeach
            </div>
        </div>
    @endif
@endif

<!-- Author -->
@if (config('raven-cms.show.lessons.author'))
    @if ($all_authors->count() > 1)
        <div class="row form-group">
            {!! Form::label('author', __('raven::messages.lessons.edit.form.author'), ['class' => 'col-md-12 control-label']) !!}
            <div class="col-md-12">
                {!! Form::select('author', $all_authors, $lesson->author, ['class' => 'form-control', 'placeholder' => 'Select one']) !!}
            </div>
        </div>
    @endif
@endif

@if (config('raven-cms.show.lessons.attachments'))
    <div class="row">
        <div class="col-md-12">
            <div id="raven-fields-ui">
                <add-by-search-editor
                    field-name="attachments"
                    :use-file-uploader="true"
                    image-path-prefix="/img"
                    field-label="Search and add attachments."
                    :initial-value="{{ $lesson->getPreppedAttachments() }}"
                    ajax-url="/admin/ravenfields/collection/attachment"
                    search-fields="title"
                    display-value-left="title"
                    lang="en"
                    display-value-right="file_type"
                    image-field-name="file_name">
                </add-by-search-editor>
            </div>
        </div>
    </div>
@endif

<!-- Image -->
@if (config('raven-cms.show.lessons.image'))
    @if ($all_images->count() > 1)
        <div class="row form-group">
            {!! Form::label('image', __('raven::messages.lessons.create.form.image'), ['class' => 'col-md-12 control-label']) !!}
            <div class="col-md-12">
                {!! Form::select('image', $all_images, $lesson->image, ['class' => 'form-control']) !!}
                
                @if (!$using_attachments)
                    <p class="small help-block">Pick one from the <a href="/admin/media" target="_blank">Media Library</a>.</p>
                @else 
                    <p class="small help-block">Pick one from the <a href="/admin/attachments" target="_blank">Attachments Library</a>.</p>
                @endif 
            </div>
        </div>
    @endif
@endif

<!-- File -->
@if (config('raven-cms.show.lessons.file'))
    @if ($all_files->count() > 1)
        <div class="row form-group">
            {!! Form::label('file', __('raven::messages.lessons.create.form.file'), ['class' => 'col-md-12 control-label']) !!}
            <div class="col-md-12">
                {!! Form::select('file', $all_files, $lesson->file, ['class' => 'form-control']) !!}
                
                @if (!$using_attachments)
                    <p class="small help-block">Pick one from the <a href="/admin/media" target="_blank">Media Library</a>.</p>
                @else 
                    <p class="small help-block">Pick one from the <a href="/admin/attachments" target="_blank">Attachments Library</a>.</p>
                @endif
            </div>
        </div>
    @endif
@endif

<!-- Video -->
@if (config('raven-cms.show.lessons.video'))
    <div class="row form-group">
        {!! Form::label('video', __('raven::messages.lessons.create.form.video'), ['class' => 'col-md-12 control-label']) !!}
        <div class="col-md-12">
            {!! Form::text('video', $lesson->video, ['class' => 'form-control']) !!}
            <p class="small help-block">{{ __('raven::messages.lessons.create.form.video_help') }}</p>
        </div>
    </div>
@endif

<!-- Quiz -->
@if (config('raven-cms.show.lessons.quiz'))
    @if ($all_quizzes->count() > 1)
        <div class="row form-group">
            {!! Form::label('quiz', __('raven::messages.lessons.edit.form.quiz'), ['class' => 'col-md-12 control-label']) !!}
            <div class="col-md-12">
                {!! Form::select('quiz', $all_quizzes, $lesson->quiz, ['class' => 'form-control']) !!}
                <p class="small help-block">Optional. This is for certification courses only.</p>
            </div>
        </div>
    @endif
@endif

<!-- Resources -->
@if (config('raven-cms.show.lessons.resources'))
    @if ($all_resources->count() > 1)
        <div class="row">
            {!! Form::label('resources', __('raven::messages.lessons.edit.form.resources'), ['class' => 'col-md-12 control-label']) !!}
        </div>
        <div class="input_resource_fields_wrap">
            @if ($resources)
                @foreach ($resources as $resource)
                    <div class="row form-group">
                        <div class="col-md-12">
                            {!! Form::select('resources[]', $all_resources, $resource, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <button class="btn btn-default add_resource_field_button marginbottom">{{ __('raven::messages.lessons.edit.form.add_resource') }}</button>
    @endif
@endif
@endsection

@section('sidebar')
<!-- Status -->
<div class="row form-group">
    {!! Form::label('status', __('raven::messages.lessons.edit.form.status'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::select('status', ['0' => 'Draft', '1' => 'Published'], $lesson->status, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Is Private -->
@if (config('raven-cms.show.lessons.is_private'))
    <div class="row form-group">
        <div class="col-md-12">
            <label>
                <input type="checkbox" name="is_private" value="1" {{ (($lesson->is_private == 1) ? 'checked' : '') }} /> {{ __('raven::messages.lessons.edit.form.is_private') }}
            </label>
        </div>
    </div>
@endif

<!-- Is Featured -->
@if (config('raven-cms.show.lessons.is_featured'))
    <div class="row form-group">
        <div class="col-md-12">
            <label>
                <input type="checkbox" name="is_featured" value="1" {{ (($lesson->is_featured == 1) ? 'checked' : '') }} /> {{ __('raven::messages.lessons.edit.form.is_featured') }}
            </label>
        </div>
    </div>
@endif

<!-- Update Button -->
<div class="row form-group">
    <div class="col-md-12">
        {!! Form::submit(__('raven::messages.lessons.edit.title'), ['class' => 'btn btn-primary btn-md margintop-sm btn-block']) !!}
    </div>
</div>
@endsection

@section('formend')
{!! Form::close() !!}
@endsection

@section('fscripts')
<script>
    $(document).ready(function() {
        var max_resource_fields      = 10; //maximum input boxes allowed
        var wrapper_resource         = $(".input_resource_fields_wrap"); //Fields wrapper
        var add_resource_button      = $(".add_resource_field_button"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_resource_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_resource_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper_resource).append('<div class="row form-group"><div class="col-md-12">{!! Form::select('resources[]', $all_resources, null, ['class' => 'form-control']) !!}</div></div>'); //add input box
            }
        });
        
        $(wrapper_resource).on("click",".remove_resource_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });

    // checkbox buttons for tags
    $(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});
</script>
@endsection
