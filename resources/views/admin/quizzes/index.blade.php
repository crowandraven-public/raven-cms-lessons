@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.quizzes.index.title'))

@section('styles')
@endsection

@section('scripts')
@endsection

@section('body-class', 'admin')

@section('content')
<div class="row">
    <div class="col-md-6">
        <h1>
            @if (isset($title))
                {{ $title }}
            @else
                {{ __('raven::messages.quizzes.index.title') }}
            @endif
            <a href="/admin/quizzes/create" class="btn btn-primary">{{ __('raven::messages.quizzes.index.add_new') }}</a>
        </h1>
    </div>
    <div class="col-md-6 text-right">
        <form class="inline form-inline" action="/admin/quizzes" method="GET">
            <div class="form-group">
                <label class="sr-only" for="search">{{ __('raven::messages.quizzes.index.search.label') }}</label>
                <input type="text" class="form-control" name="search" id="search" placeholder="{{ __('raven::messages.quizzes.index.search.placeholder') }}">
            </div>
        </form>
    </div>
</div>
<!-- Quizzes List -->
<div class="row">
    <div class="col-md-12 margintop-sm">
        @if ($quizzes)
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">
                            <strong>{{ __('raven::messages.quizzes.index.table.title') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.quizzes.index.table.updated') }}</strong>
                        </th>
                        <th width="50" scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($quizzes as $quiz)
                        <tr>
                            <th scope="row">
                                <a href="/admin/quizzes/{{ $quiz->id }}/edit">{{ $quiz->title }}</a>
                            </th>
                            <td>
                                {{ $quiz->updated_at->format('M j, Y g:i:s a') }}
                            </td>
                            <td>
                                <button class="btn btn-danger-outline btn-delete" data-toggle="modal" data-target="#delete-{{ $quiz->id }}">
                                    <i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="Delete {{ $quiz->title }}"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
    <div class="col-sm-12">
        {{ $quizzes->links() }}
    </div>
</div>

@foreach ($quizzes as $quiz)
    <!-- Delete Modal -->
    <div class="modal fade" id="delete-{{ $quiz->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ __('raven::messages.quizzes.index.table.delete') }}</h4>
                </div>
                <div class="modal-body">
                    @include('raven::admin.quizzes.partials.delete')
                </div>
            </div>
        </div>
    </div>
@endforeach
@endsection

@section('fscripts')
@endsection
