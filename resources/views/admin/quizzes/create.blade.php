@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.quizzes.create.title'))

@section('scripts')
<script src="https://cdn.tiny.cloud/1/va0cczdbx8gc8utla2gv66pu2ip8nq2yanvgzp9sq0g1bvsj/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({
    selector:'textarea',
    height: 300,
    theme: 'silver',
    plugins: [
        'advlist autolink lists link image charmap hr anchor',
        'searchreplace wordcount visualblocks visualchars code',
        'media nonbreaking save table contextmenu directionality',
        'paste imagetools'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
    menubar: 'tools',
    image_advtab: true,
});</script>
@endsection

@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/quizzes', 'role' => 'form', 'method' => 'post']) !!}
@endsection

@section('content')
<h1>{{ __('raven::messages.quizzes.create.title') }}</h1>
        	
<!-- Title -->
<div class="row form-group">
    {!! Form::label('title', __('raven::messages.quizzes.create.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<!-- Content -->
<div class="row form-group">
    {!! Form::label('content', __('raven::messages.quizzes.create.form.content'), ['class' => 'col-md-12 control-label']) !!}
    <div class="col-md-12">
        {!! Form::textarea('content', null, ['class' => 'form-control','rows' => '7']) !!}
    </div>
</div>

<div class="row form-group">
    <div class="col-md-12 margintop-xs">
        <div id="raven-fields-ui">
            <quiz-editor 
                button-label="Add Question" 
                :enable-hint-field="true">
            </quiz-editor>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h3>{{ __('raven::messages.quizzes.create.form.meta') }}</h3>
    </div>
</div>

<!-- Track Tags -->
@if ($all_tracks->count() > 0)
    <div class="row form-group">
        {!! Form::label('track_tags', __('raven::messages.quizzes.create.form.tracks'), ['class' => 'col-md-12 control-label required']) !!}
        
        <div class="tags-container">
            @foreach ($all_tracks as $key => $value)
                <span class="button-checkbox">
                    <button type="button" class="btn btn-tag" data-color="primary">{{ str_replace('"', '', $value) }}</button>
                    <input type="checkbox" class="hidden" name="track_tags[]" value="{{ $key }}" />
                </span>
            @endforeach
        </div>
    </div>
@endif

<!-- Topic Tags -->
@if ($all_topics->count() > 0)
    <div class="row form-group">
        {!! Form::label('topic_tags', __('raven::messages.quizzes.create.form.topics'), ['class' => 'col-md-12 control-label']) !!}
        
        <div class="tags-container">
            @foreach ($all_topics as $key => $value)
                <span class="button-checkbox">
                    <button type="button" class="btn btn-tag" data-color="primary">{{ str_replace('"', '', $value) }}</button>
                    <input type="checkbox" class="hidden" name="topic_tags[]" value="{{ $key }}" />
                </span>
            @endforeach
        </div>
    </div>
@endif

<!-- Locale -->
@if (is_array(config('raven-cms.locale')))
    <div class="row form-group">
        {!! Form::label('locale', __('raven::messages.quizzes.create.form.locale'), ['class' => 'col-md-12 control-label required']) !!}
        <div class="col-md-12">
            {!! Form::select('locale', config('raven-cms.locale'), config('raven-cms.fallback_locale'), ['class' => 'form-control']) !!}
        </div>
    </div>
@else
    {!! Form::hidden('locale', config('raven-cms.fallback_locale')) !!}
@endif
@endsection

@section('sidebar')
<!-- Status -->
<div class="row form-group">
    {!! Form::label('status', __('raven::messages.quizzes.create.form.status'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::select('status', ['0' => 'Draft', '1' => 'Published'], 1, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Create Button -->
<div class="row form-group">
    <div class="col-md-12">
        {!! Form::submit(__('raven::messages.quizzes.create.title'), ['class' => 'btn btn-primary btn-md margintop-sm btn-block']) !!}
    </div>
</div>
@endsection

@section('formend')
{!! Form::close() !!}
@endsection

@section('fscripts')
<script>
    // checkbox buttons for tags
    $(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});
</script>
@endsection
