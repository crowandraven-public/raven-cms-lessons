@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.series.index.title'))

@section('styles')
@endsection

@section('scripts')
@endsection

@section('body-class', 'admin')

@section('content')
<div class="row">
    <div class="col-md-6">
        <h1>
            @if (isset($title))
                {{ $title }}
            @else
                {{ __('raven::messages.series.index.title') }}
            @endif
            <a href="/admin/series/create" class="btn btn-primary">{{ __('raven::messages.series.index.add_new') }}</a>
        </h1>
    </div>
    <div class="col-md-6 text-right">
        <form class="inline form-inline" action="/admin/series" method="GET">
            <div class="form-group">
                <label class="sr-only" for="search">{{ __('raven::messages.series.index.search.label') }}</label>
                <input type="text" class="form-control" name="search" id="search" placeholder="{{ __('raven::messages.series.index.search.placeholder') }}">
            </div>
        </form>
    </div>
</div>
<!-- Series List -->
<div class="row">
    <div class="col-md-12 margintop-sm">
        @if ($series_array)
            <table class="table table-hover">
                <thead>
                    <tr>
                        @if (config('raven-cms.show.series.is_featured'))
                            <th width="50" scope="col">
                                <strong>{{ __('raven::messages.series.index.table.featured') }}</strong>
                            </th>
                        @endif
                        @if (config('raven-cms.show.series.is_private'))
                            <th width="50" scope="col">
                                <strong><i class="fas fa-lock fa-fw"></i></strong>
                            </th>
                        @endif
                        <th scope="col">
                            <strong>{{ __('raven::messages.series.index.table.title') }}</strong>
                        </th>
                        <th scope="col">
                            <strong>{{ __('raven::messages.series.index.table.updated') }}</strong>
                        </th>
                        <th width="50" scope="col"></th>
                    </tr>
                </thead>
                <tbody>

                    @foreach ($series_array as $series)
                        <tr>
                            @if (config('raven-cms.show.series.is_featured'))
                                <th scope="row">
                                    @if ($series->is_featured)
                                        <i class="fas fa-check"></i>
                                    @endif
                                </th>
                            @endif
                            @if (config('raven-cms.show.series.is_private'))
                                <td>
                                    @if ($series->is_private)
                                        <i class="fas fa-check"></i>
                                    @endif
                                </td>
                            @endif
                            <td>
                                <a href="/series/{{ localize($series->slug) }}" target="_blank"><i class="fas fa-external-link-alt fa-fw"></i></a> <a href="/admin/series/{{ $series->id }}/edit">{{ localize($series->title) }}</a>
                            </td>
                            <td>
                                {{ $series->updated_at->toDayDateTimeString() }}
                            </td>
                            <td>
                                <button class="btn btn-danger-outline btn-delete" data-toggle="modal" data-target="#delete-{{ $series->id }}">
                                    <i class="fa fa-times" data-toggle="tooltip" data-placement="top" title="Delete {{ localize($series->title) }}"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>
    <div class="col-sm-12">
        {{ $series_array->links() }}
    </div>
</div>

@foreach ($series_array as $series)
    <!-- Delete Modal -->
    <div class="modal fade" id="delete-{{ $series->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ __('raven::messages.series.index.table.delete') }}</h4>
                </div>
                <div class="modal-body">
                    @include('raven::admin.series.partials.delete')
                </div>
            </div>
        </div>
    </div>
@endforeach
@endsection

@section('fscripts')
@endsection
