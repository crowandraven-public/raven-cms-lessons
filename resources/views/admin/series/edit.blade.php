@extends('raven::admin.layouts.admin')
@section('title', __('raven::messages.series.edit.title'))

@section('scripts')
<script src="https://cdn.tiny.cloud/1/va0cczdbx8gc8utla2gv66pu2ip8nq2yanvgzp9sq0g1bvsj/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>tinymce.init({
    selector:'textarea',
    height: 300,
    theme: 'silver',
    plugins: [
        'advlist autolink lists link image charmap hr anchor',
        'searchreplace wordcount visualblocks visualchars code',
        'media nonbreaking save table contextmenu directionality',
        'paste imagetools'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
    menubar: 'tools',
    image_advtab: true,
});</script>
@endsection

@section('body-class', 'admin')

@section('formstart')
{!! Form::open(['url' => 'admin/series/'.$series->id, 'role' => 'form', 'method' => 'put']) !!}
@endsection

@section('content')
<h1>{{ __('raven::messages.series.edit.title') }}</h1>

<div class="row">
    <div class="col-md-12 margintop-xs marginbottom-xs">
        <a href="{{ env('APP_URL') }}/series/{{ localize($series->slug) }}">{{ env('APP_URL') }}/series/{{ localize($series->slug) }}</a>
    </div>
</div>

<!-- Title -->
<div class="row form-group">
    {!! Form::label('title', __('raven::messages.series.edit.form.title'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('title-'.$locale, localize($series->title, $locale), ['class' => 'form-control', 'required' => 'required']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('title-'.config('raven-cms.fallback_locale'), localize($series->title, config('raven-cms.fallback_locale')), ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    @endif
</div>

<!-- Slug -->
<div class="row form-group">
    {!! Form::label('slug', __('raven::messages.series.edit.form.slug'), ['class' => 'col-md-12 control-label required']) !!}
    @if (is_array(config('raven-cms.locale')))
        <div class="panel panel-fieldset">
            <div class="panel-body">
                @foreach (config('raven-cms.locale') as $locale)
                    <div class="col-md-12">
                        {!! Form::text('slug-'.$locale, localize($series->slug, $locale), ['class' => 'form-control']) !!}
                        <p class="small help-block">{{ $locale }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @else
        <div class="col-md-12">
            {!! Form::text('slug-'.config('raven-cms.fallback_locale'), localize($series->slug, config('raven-cms.fallback_locale')), ['class' => 'form-control']) !!}
        </div>
    @endif
</div>

<!-- Content -->
@if (config('raven-cms.show.series.content'))
    <div class="row form-group">
        {!! Form::label('content', __('raven::messages.series.edit.form.content'), ['class' => 'col-md-12 control-label']) !!}
        @if (is_array(config('raven-cms.locale')))
            <div class="panel panel-fieldset">
                <div class="panel-body">
                    @foreach (config('raven-cms.locale') as $locale)
                        <div class="col-md-12">
                            {!! Form::textarea('content-'.$locale, localize($series->content, $locale), ['class' => 'form-control','rows' => '5']) !!}
                            <p class="small help-block">{{ $locale }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="col-md-12">
                {!! Form::textarea('content-'.config('raven-cms.fallback_locale'), localize($series->content, config('raven-cms.fallback_locale')), ['class' => 'form-control','rows' => '5']) !!}
            </div>
        @endif
    </div>
@endif

<div class="row">
    <div class="col-md-12">
        <h3>{{ __('raven::messages.series.edit.form.meta') }}</h3>
    </div>
</div>

<!-- Excerpt -->
@if (config('raven-cms.show.series.excerpt'))
    <div class="row form-group">
        {!! Form::label('excerpt', __('raven::messages.series.edit.form.excerpt'), ['class' => 'col-md-12 control-label required']) !!}
        @if (is_array(config('raven-cms.locale')))
            <div class="panel panel-fieldset">
                <div class="panel-body">
                    @foreach (config('raven-cms.locale') as $locale)
                        <div class="col-md-12">
                            {!! Form::text('excerpt-'.$locale, localize($series->excerpt, $locale), ['class' => 'form-control']) !!}
                            <p class="small help-block">{{ $locale }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="col-md-12">
                {!! Form::text('excerpt-'.config('raven-cms.fallback_locale'), localize($series->excerpt, config('raven-cms.fallback_locale')), ['class' => 'form-control']) !!}
            </div>
        @endif
    </div>
@endif

<!-- Recommended For -->
@if (config('raven-cms.show.series.recommended_for'))
    <div class="row form-group">
        {!! Form::label('recommended_for', __('raven::messages.series.edit.form.recommended_for'), ['class' => 'col-md-12 control-label']) !!}
        @if (is_array(config('raven-cms.locale')))
            <div class="panel panel-fieldset">
                <div class="panel-body">
                    @foreach (config('raven-cms.locale') as $locale)
                        <div class="col-md-12">
                            {!! Form::text('recommended_for-'.$locale, localize($series->recommended_for, $locale), ['class' => 'form-control']) !!}
                            <p class="small help-block">{{ $locale }}</p>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="col-md-12">
                {!! Form::text('recommended_for-'.config('raven-cms.fallback_locale'), localize($series->recommended_for, config('raven-cms.fallback_locale')), ['class' => 'form-control']) !!}
            </div>
        @endif
    </div>
@endif

<!-- Config -->
@if (config('raven-cms.show.series.config'))
    <div class="row form-group">
        {!! Form::label('config', __('raven::messages.series.edit.form.config'), ['class' => 'col-md-12 control-label']) !!}
        <div class="col-md-12">
            {!! Form::text('config', $series->config, ['class' => 'form-control']) !!}
            <p class="small help-block">It currently accepts any valid JSON.</p>
        </div>
    </div>
@endif

<!-- Testimony Text -->
@if (config('raven-cms.show.series.testimony'))
    <div class="row form-group">
        {!! Form::label('testimony_text', __('raven::messages.series.edit.form.testimony_text'), ['class' => 'col-md-12 control-label']) !!}
        <div class="col-md-12">
            {!! Form::text('testimony_text', $series->testimony_text, ['class' => 'form-control','rows' => '4']) !!}
            <p class="small help-block">Don't include quote marks—just the text.</p>
        </div>
    </div>

    <!-- Testimony Attribution -->
    <div class="row form-group">
        {!! Form::label('testimony_attrib', __('raven::messages.series.edit.form.testimony_attrib'), ['class' => 'col-md-12 control-label']) !!}
        <div class="col-md-12">
            {!! Form::text('testimony_attrib', $series->testimony_attrib, ['class' => 'form-control']) !!}
            <p class="small help-block">Example: Bob Smith, Sargent, Boston Police Department</p>
        </div>
    </div>
@endif

<!-- Track Tags -->
@if (config('raven-cms.show.series.track_tags'))
    @if ($all_tracks->count() > 0)
        <div class="row form-group">
            {!! Form::label('track_tags', __('raven::messages.series.edit.form.tracks'), ['class' => 'col-md-12 control-label required']) !!}
            
            <div class="tags-container">
                @foreach ($all_tracks as $key => $value)
                    <span class="button-checkbox">
                        <button type="button" class="btn btn-tag" data-color="primary">{{ str_replace('"', '', $value) }}</button>
                        <input type="checkbox" class="hidden" name="track_tags[]" value="{{ $key }}" {{ (in_array($key, $series_tags->toArray()) ? 'checked' : '') }} />
                    </span>
                @endforeach
            </div>
        </div>
    @endif
@endif

<!-- Topic Tags -->
@if (config('raven-cms.show.series.topic_tags'))
    @if ($all_topics->count() > 0)
        <div class="row form-group">
            {!! Form::label('topic_tags', __('raven::messages.series.edit.form.topics'), ['class' => 'col-md-12 control-label']) !!}
            
            <div class="tags-container">
                @foreach ($all_topics as $key => $value)
                    <span class="button-checkbox">
                        <button type="button" class="btn btn-tag" data-color="primary">{{ str_replace('"', '', $value) }}</button>
                        <input type="checkbox" class="hidden" name="topic_tags[]" value="{{ $key }}" {{ (in_array($key, $series_tags->toArray()) ? 'checked' : '') }} />
                    </span>
                @endforeach
            </div>
        </div>
    @endif
@endif

<!-- Author -->
@if (config('raven-cms.show.series.author'))
    @if ($all_authors->count() > 1)
        <div class="row form-group">
            {!! Form::label('author', __('raven::messages.series.edit.form.author'), ['class' => 'col-md-12 control-label']) !!}
            <div class="col-md-12">
                {!! Form::select('author', $all_authors, $series->author, ['class' => 'form-control']) !!}
            </div>
        </div>
    @endif
@endif

<!-- Image -->
@if (config('raven-cms.show.series.image'))
    @if ($all_images->count() > 1)
        <div class="row form-group">
            {!! Form::label('image', __('raven::messages.series.edit.form.image'), ['class' => 'col-md-12 control-label required']) !!}
            <div class="col-md-12">
                {!! Form::select('image', $all_images, $series->image, ['class' => 'form-control']) !!}
                
                @if (!$using_attachments)
                    <p class="small help-block">Pick one from the <a href="/admin/media" target="_blank">Media Library</a>.</p>
                @else
                    <p class="small help-block">Pick one from the <a href="/admin/attachments" target="_blank">Attachments Library</a>.</p>
                @endif
            </div>
        </div>
    @endif
@endif

<!-- Video -->
@if (config('raven-cms.show.series.video'))
    <div class="row form-group">
        {!! Form::label('video', __('raven::messages.series.edit.form.video'), ['class' => 'col-md-12 control-label']) !!}
        <div class="col-md-12">
            {!! Form::text('video', $series->video, ['class' => 'form-control']) !!}
            <p class="small help-block">{{ __('raven::messages.series.edit.form.video_help') }}</p>
        </div>
    </div>
@endif

@if (config('raven-cms.show.series.add_lessons'))
    @if ($all_lessons->count() > 1)
        <div class="row">
            <h3>{!! Form::label('lessons', __('raven::messages.series.edit.form.lessons'), ['class' => 'col-md-12 control-label']) !!}</h3>
        </div>
        <div class="input_lesson_fields_wrap">
            @if ($lessons)
                @foreach ($lessons as $lesson)
                    <div class="row form-group">
                        <div class="col-md-11">
                            {!! Form::select('lessons[]', $all_lessons, $lesson, ['class' => 'form-control']) !!}
                        </div>
                        <div class="col-md-1 text-center">
                            {{ $loop->iteration }}
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <button class="btn btn-default add_lesson_field_button marginbottom">{{ __('raven::messages.series.edit.form.add_lesson') }}</button>
    @endif
@endif
@endsection


@section('sidebar')
<!-- Status -->
<div class="row form-group">
    {!! Form::label('status', __('raven::messages.series.edit.form.status'), ['class' => 'col-md-12 control-label required']) !!}
    <div class="col-md-12">
        {!! Form::select('status', ['0' => 'Draft', '1' => 'Published'], $series->status, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Is Private -->
@if (config('raven-cms.show.series.is_private'))
    <div class="row form-group">
        <div class="col-md-12">
            <label>
                <input type="checkbox" name="is_private" value="1" {{ (($series->is_private == 1) ? 'checked' : '') }} /> {{ __('raven::messages.series.edit.form.is_private') }}
            </label>
        </div>
    </div>
@endif

<!-- Is Featured -->
@if (config('raven-cms.show.series.is_featured'))
    <div class="row form-group">
        <div class="col-md-12">
            <label>
                <input type="checkbox" name="is_featured" value="1" {{ (($series->is_featured == 1) ? 'checked' : '') }} /> {{ __('raven::messages.series.edit.form.is_featured') }}
            </label>
        </div>
    </div>
@endif

<!-- Update Button -->
<div class="row form-group">
    <div class="col-md-12">
        {!! Form::submit(__('raven::messages.series.edit.title'), ['class' => 'btn btn-primary btn-md margintop-sm btn-block']) !!}
    </div>
</div>

{!! Form::close() !!}

<div class="row form-group">
    <div class="col-md-12">
        {!! Form::open(['url' => 'series/'.$series->id.'/copy', 'class' => 'form-duplicate-series-'.$series->id, 'role' => 'form', 'id' => 'seriesDuplicateForm']) !!}
            {!! Form::submit(__('raven::messages.series.edit.form.duplicate'), ['class' => 'btn btn-default margintop-sm']) !!}
        {!! Form::close() !!}
    </div>
</div>
@endsection


@section('formend')


@endsection


@section('fscripts')
<script>
    $(document).ready(function() {
        var max_lesson_fields        = 50; //maximum input boxes allowed
        var wrapper_lesson           = $(".input_lesson_fields_wrap"); //Fields wrapper
        var add_lesson_button        = $(".add_lesson_field_button"); //Add button ID
        
        var x = 1; //initlal text box count
        $(add_lesson_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_lesson_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper_lesson).append('<div class="row form-group"><div class="col-md-12">{!! Form::select('lessons[]', $all_lessons, null, ['class' => 'form-control']) !!}</div></div>'); //add input box
            }
        });
        
        $(wrapper_lesson).on("click",".remove_lesson_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })
    });

    // checkbox buttons for tags
    $(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});
</script>
@endsection
