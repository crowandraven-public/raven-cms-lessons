@extends('raven::layouts.app-novue')

@section('title', localize($page->title))
@section('description', localize($page->excerpt))
@section('image', env('APP_URL').$page->image, env('APP_URL').'/img/opengraph-1200x630.jpg')

@section('body-class', 'catalog page')

@section('content')
<div class="catalog-nav">
    <div class="container">
        @include('raven::catalog.partials.nav')
    </div>
</div>

@include('raven::catalog.partials.hero', ['title' => $page->title, 'subtitle' => $page->subtitle])

<div class="container">
    @if (!Auth::user() || (!Auth::user()->likedLessons && !Auth::user()->completedLessons))
        <div class="row">
            <div class="col-sm-12 margintop">
                There's nothing to see here at the moment.
            </div>
        </div>
    @endif

    @if (isset(Auth::user()->likedLessons))
        <div class="row">
            <div class="col-sm-12">
                <h2>Saved Lessons</h2>
            </div>
        </div>
        <div class="row">
            @include('raven::lessons.partials.cards', ['lessons' => Auth::user()->likedLessons, 'type' => 'lessons'])
        </div>
    @endif

    @if (isset(Auth::user()->completedLessons))
        <div class="row">
            <div class="col-sm-12">
                <h2>Completed Lessons</h2>
            </div>
        </div>
        <div class="row">
            @include('raven::lessons.partials.cards', ['lessons' => Auth::user()->completedLessons, 'type' => 'lessons'])
        </div>
    @endif

    @if (isset(Auth::user()->likedSeries))
        <div class="row">
            <div class="col-sm-12">
                <h2>Saved Series</h2>
            </div>
        </div>
        <div class="row">
            @include('raven::series.partials.cards', ['seriess' => Auth::user()->likedSeries, 'type' => 'series'])
        </div>
    @endif
    
    @if (isset(Auth::user()->completedSeries))
        <div class="row">
            <div class="col-sm-12">
                <h2>Completed Series</h2>
            </div>
        </div>
        <div class="row">
            @include('raven::series.partials.cards', ['seriess' => Auth::user()->completedSeries, 'type' => 'series'])
        </div>
    @endif


    @if (isset(Auth::user()->likedPosts))
        <div class="row">
            <div class="col-sm-12">
                <h2>Saved Webinars</h2>
            </div>
        </div>
        <div class="row">
            @include('raven::webinars.partials.cards', ['posts' => Auth::user()->likedPosts, 'type' => 'posts'])
        </div>
    @endif
      

</div>
@endsection

@section('fscripts')
<script src="{{ elixir('js/plugins.js') }}"></script>
<script>
$(function(){
    var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        getSortData: {
            category: '[data-category]'
        }
    });

    $(function(){
        if(window.location.hash.length) {
            $grid.isotope({ filter: window.location.hash.replace(/#/, '.') });
        }
    });

    window.addEventListener("hashchange", function(){
        $grid.isotope({ filter: window.location.hash.replace(/#/, '.') });
    });

    // bind filter button click
    $('#filters-topic').on('click', '.filter', function() {
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({ filter: filterValue });
    });
});
</script>
@endsection
