<ul id="catalog-nav" class="nav nav-tabs nav-justified" role="tablist">
    <li {{ (Request::is('lessons*') || Request::is('catalog*')) ? ' class=active' : '' }}><a href="/lessons">Lessons</a></li>
    <li {{ Request::is('series*') ? ' class=active' : '' }}><a href="/series">Series</a></li>
    <li {{ Request::is('courses*') ? ' class=active' : '' }}><a href="/courses">Courses</a></li>
    <li {{ Request::is('webinars*') ? ' class=active' : '' }}><a href="/webinars">Webinars</a></li>
    @if (Auth::user())
    	<li {{ Request::is('my-evenpulse*') ? ' class=active' : '' }}><a href="/my-evenpulse">My {{ Config::get('app.name') }}</a></li>
    @endif
</ul>
