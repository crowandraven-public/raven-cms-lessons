<div class="trial-banner">
	<div class="container text-center">
		<h2>Discover great content, delivered to you.</h2>
		<a href="/settings#/login" class="btn btn-md btn-default">Login</a>
		or
		<a href="/settings#/register" class="btn btn-md btn-default">Register</a>
	</div>

</div>

