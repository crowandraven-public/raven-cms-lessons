<div class="row">
    <div class="col-md-4">
        <a href="/plans#custom">
            <div class="service-item">
                <div class="service-item-thumb" style="background: url('/img/php02OJjH.jpg'); background-repeat: no-repeat; background-size: cover;"></div>
                <div class="service-item-content related-content">
                    <h3>Onsite Training</h3>
                    <div class="service-item-excerpt">
                        Have EVENPULSE training on location for leadership and teams.
                    </div>
                </div>
                <button class="btn btn-primary btn-md btn-block btn-learn-more">Learn More</button>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="/plans#custom">
            <div class="service-item">
                <div class="service-item-thumb" style="background: url('/img/phpVKDTcw.jpg'); background-repeat: no-repeat; background-size: cover;"></div>
                <div class="service-item-content related-content">
                    <h3>One-on-One Coaching</h3>
                    <div class="service-item-excerpt">
                        Get personalized coaching and a training plan tailored to your needs.
                    </div>
                </div>
                <button class="btn btn-primary btn-md btn-block btn-learn-more">Learn More</button>
            </div>
        </a>
    </div>
    <div class="col-md-4">
        <a href="/plans#contact">
            <div class="service-item">
                <div class="service-item-thumb" style="background: url('/img/phpgDbObP.jpg'); background-repeat: no-repeat; background-size: cover;"></div>
                <div class="service-item-content related-content">
                    <h3>Not Sure Which Is Best?</h3>
                    <div class="service-item-excerpt">
                        Depending on your goals, the size of your team, and your time available, we will tailor EVENPULSE training to your needs.
                    </div>
                </div>
                <button class="btn btn-primary btn-md btn-block btn-learn-more">Contact Us</button>
            </div>
        </a>
    </div>
</div>