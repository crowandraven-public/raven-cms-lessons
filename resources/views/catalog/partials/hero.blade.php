<div class="hero no-hero">
    <div class="row container hero-content">
        <div class="col-md-6 title-container">
            <h1>{{ localize($page->title) }}</h1>
            @if (localize($page->subtitle))
                <div class="sub-heading">
                    {{ localize($page->subtitle) }}
                </div>
            @endif
        </div>
    </div>
</div>
