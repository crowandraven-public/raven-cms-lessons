@extends('raven::layouts.app-novue')

@section('title', localize($page->title))
@section('description', localize($page->excerpt))
@section('image', env('APP_URL').$page->image, env('APP_URL').'/img/opengraph-1200x630.jpg')

@section('body-class', 'catalog page')

@section('content')
<div class="catalog-nav">
    <div class="container">
        @include('raven::catalog.partials.nav')
    </div>
</div>

@include('raven::catalog.partials.hero')

<div class="container">
    @if (isset($lessons))
        <div class="row margintop">
            @include('raven::lessons.partials.cards', ['type' => 'lessons'])
        </div>
    @endif
</div>
@endsection

@section('fscripts')
<script src="{{ elixir('js/plugins.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script>
$(function(){
    var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        getSortData: {
            category: '[data-category]'
        }
    });

    $(function(){
        if(window.location.hash.length) {
            $grid.isotope({ filter: window.location.hash.replace(/#/, '.') });
        }
    });

    window.addEventListener("hashchange", function(){
        $grid.isotope({ filter: window.location.hash.replace(/#/, '.') });
    });

    // bind filter button click
    $('#filters-topic').on('click', '.filter', function() {
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({ filter: filterValue });
    });
});
</script>
@endsection
