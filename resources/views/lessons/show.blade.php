@extends('raven::layouts.app-novue')

@section('title', localize($lesson->title))
@section('description', localize($lesson->excerpt))
@section('image', env('APP_URL').$lesson->image, env('APP_URL').'/img/opengraph-1200x630.jpg')

@section('body-class', 'catalog page lesson')

@section('content')
<home :user="user" inline-template>
    <div class="catalog-nav">
        <div class="container">
            @include('raven::catalog.partials.nav')
        </div>
    </div>

    @if (is_free($lesson) || Auth::user()->subscribed())
        {{-- show lesson to subscribed user --}}
        <div class="dark-band-video">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row no-gutter no-row-margin video-row">
                            <div class="col-md-12">
                                @if ($lesson->video)
                                    <div class="video embed-responsive embed-responsive-16by9">
                                        <iframe id="video-main" class="embed-responsive-item" src="{{ $lesson->video }}" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                    </div>
                                @else
                                    <div class="photo-placeholder">
                                        <img src="{{ asset('/img/'.$lesson->image) }}" alt="{{ localize($lesson->title) }}" class="img-responsive" width="100%">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        {{-- show placeholder if not subscribed --}}
        <div class="dark-band-video">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row no-gutter no-row-margin video-row">
                            <div class="col-md-12">
                                <div class="photo-placeholder">
                                    <img src="{{ asset('/img/'.$lesson->image) }}" alt="{{ localize($lesson->title) }}" class="img-responsive" width="100%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-8 main-content">
                <h1>{{ localize($lesson->title) }}</h1>
                
                <div class="meta">
                    <a href="/lessons"><i class="fas fa-angle-left"></i> View All Lessons</a> &middot;

                    {{--@include('raven::lessons.partials.tags')--}}

                    Run Time {{ gmdate("i:s", $lesson->length) }}
                </div>

                @include('raven::lessons.partials.content')
            </div>
            <div class="col-md-4 margintop-sm">
                <div class="video-row-buttons marginbottom">
                    @if (is_free($lesson) || Auth::user()->subscribed())
                        @include('raven::lessons.partials.complete')
                        @include('raven::lessons.partials.save')
                    @else
                        <a href="/settings#/subscription"><i class="fas fa-lock fa-fw fa-2x" data-toggle="tooltip" data-placement="top" title data-original-title="Unlock this Lesson"></i></a>
                    @endif
                    
                    @include('raven::lessons.partials.share')
                </div>

                @include('raven::lessons.partials.resources')

                @include('raven::lessons.partials.author')
            </div>
        </div>
    </div>
</div>
    <div class="container">
        <div class="row">
            {{-- show related lessons, if they exist --}}
            @if (isset($lessons) && is_var_iterable($lessons))
                <div class="col-md-12">
                    <h3>You might also enjoy...</h3>
                </div>
                <div class="col-sm-12">
                    <h4 class="section-title">Related Lessons</h4>
                </div>
                @include('raven::lessons.partials.cards')
            @endif
        </div>
    </div>
</home>
@endsection

@section('fscripts')
<script src="{{ elixir('js/plugins.js') }}"></script>
<script>
    $(document).ready(function(){
        var $grid = $('.grid').isotope({
            itemSelector: '.grid-item',
            columnWidth: '.grid-sizer',
            percentPosition: true,
            getSortData: {
                category: '[data-category]'
            }
        });
        $("#share-button-active").hide();
        $("#share-button").show();
        if ($(window).width() < 960) {
            $(".share").click(function(e){
                e.preventDefault();
                $("#share-button-active").show();
                $("#share-button").hide();
            });
        } else {
            $(".share").hover(function(e){
                e.preventDefault();
                $("#share-button-active").show();
                $("#share-button").hide();
            });
        }
    });
</script>
@endsection
