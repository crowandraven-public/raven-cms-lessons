@if (isset($tags) && is_var_iterable($tags))
    <span class="topics tags">
        <i class="fas fa-tag fa-fw"></i>
        @foreach($tags as $tag)
            <a href="/lessons#{{ localize($tag->slug) }}">{{ ucwords(localize($tag->name)) }}</a>{{ !$loop->last ? ',' : '' }}
        @endforeach
        &middot;
    </span>
@endif
