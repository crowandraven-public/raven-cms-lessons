@if ($lessons->count() > 0)
    <div class="col-sm-12">
        <div id="grid" class="grid lessons-grid" style="margin-left: -1.5rem; margin-right: -1.5rem;">
            <div class="grid-sizer col-sm-6 col-md-4"></div>
            @foreach($lessons as $lesson)
                @php
                    $tags = '';
                    if (isset($lesson->tags)) {
                        foreach ($lesson->tags as $tag) {
                            $tags .= localize($tag->slug).' ';
                        }
                        $tags = trim($tags);
                    }

                    $config = $lesson->config;
                @endphp

                <div class="grid-item col-sm-6 col-md-4 all {{ $tags }}">
                    
                    <div class="card">
                        <div class="card-bg">
                            <a href="/{{ (isset($type) ? $type : 'lessons') }}/{{ localize($lesson->slug) }}">
                                <div class="image-bg" style="background-image: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('{{ isset($lesson->image) ? '/img/'.$lesson->image.'?w=600&h=400&fit=crop&q=60' : 'http://placehold.it/600x400' }}'); background-size: cover; background-repeat: no-repeat;">
                                    <div class="tags">
                                        @if (is_free($lesson))
                                            <div class="card-free">
                                                <span class="card-free-content">Free</span>
                                            </div>
                                        @else
                                            @if (!is_subscribed_basic(Auth::user()))
                                                <span data-toggle="tooltip" data-placement="top" title="Upgrade to Unlock"><i class="fa fas fa-lock fa-fw"></i></span>
                                            @endif
                                        @endif

                                        @if ($lesson->created_at->gt(\Carbon\Carbon::now()->subDays(30)))
                                            <div class="card-new">
                                                <span class="card-new-content">New</span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="title">
                                    <h3>{{ mb_strimwidth(localize($lesson->title), 0, 150, "...") }}</h3>
                                </div>
                            </a>
                    
                            <div class="actions push-right">
                                @if (Auth::user()->subscribed())

                                    @component('raven::partials.likeable', [
                                        'likeable_action_url' => '/lesson/complete/'.$lesson->id,
                                        'post_type_name' => 'Lesson',
                                        'liked' => (Auth::user()->completedLessons && Auth::user()->completedLessons->contains('id', $lesson->id)) ? '1': '0',
                                        'liked_class' => 'fa fa-check fa-fw pointer',
                                        'not_liked_class' => 'fa fa-check fa-fw pointer',
                                        'tooltip_title_liked' => 'Lesson Completed',
                                        'tooltip_title_not_liked' => 'Mark as Completed'
                                    ])
                                    @endcomponent

                                    @component('raven::partials.likeable', [
                                        'likeable_action_url' => '/lesson/like/'.$lesson->id,
                                        'post_type_name' => 'Lesson',
                                        'liked' => (Auth::user()->likedLessons && Auth::user()->likedLessons->contains('id', $lesson->id)) ? '1': '0',
                                        'liked_class' => 'fa fa-bookmark fa-fw pointer',
                                        'not_liked_class' => 'fa fa-bookmark fa-fw pointer',
                                        'tooltip_title_liked' => 'Saved',
                                        'tooltip_title_not_liked' => 'Save'
                                    ])
                                    @endcomponent
                                    
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach

        </div>
    </div>

    @if (method_exists($lessons, 'links'))
        <div class="col-sm-12">
            {{ $lessons->links() }}
        </div>
    @endif
@else
    <p>There are currently no lessons available.</p>
@endif
