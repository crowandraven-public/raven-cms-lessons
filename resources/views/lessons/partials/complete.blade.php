@if (Auth::user()->subscribed())
    <div class="inline fa-2x">
        @component('raven::partials.likeable', [
            'likeable_action_url' => '/lesson/complete/'.$lesson->id,
            'post_type_name' => 'Lesson',
            'liked' => (Auth::user()->completedLessons && Auth::user()->completedLessons->contains('id', $lesson->id)) ? '1': '0',
            'liked_class' => 'far fa-check-circle fa-fw completed pointer',
            'not_liked_class' => 'far fa-check-circle fa-fw pointer',
            'tooltip_title_liked' => 'Lesson Completed',
			'tooltip_title_not_liked' => 'Mark as Completed'
        ])
        @endcomponent
    </div>
@endif