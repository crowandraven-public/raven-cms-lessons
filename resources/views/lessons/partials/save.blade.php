@if (Auth::user()->subscribed())
	<div class="inline fa-2x">
		@component('raven::partials.likeable', [
			'likeable_action_url' => '/lesson/like/'.$lesson->id,
			'post_type_name' => 'Lesson',
			'liked' => (Auth::user()->likedLessons && Auth::user()->likedLessons->contains('id', $lesson->id)) ? '1': '0',
			'liked_class' => 'fas fa-bookmark fa-fw saved pointer',
			'not_liked_class' => 'fas fa-bookmark fa-fw pointer',
			'tooltip_title_liked' => 'Lesson Saved',
			'tooltip_title_not_liked' => 'Save this Lesson'
		])
		@endcomponent
	</div>
@endif