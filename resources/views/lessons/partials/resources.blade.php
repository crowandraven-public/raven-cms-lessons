@if (is_free($lesson) || Auth::user()->subscribed())
	@if (is_var_iterable($lesson->resources))
	    <div class="marginbottom">
	        <h3 class="section-title">Related Resources</h3>
	        <ul>
	        	@foreach ($lesson->resources as $resource)
	            	<li><a href="{{ $resource->url }}">{{ localize($resource->title) }}</a></li>
	        	@endforeach
	        </ul>
	    </div>
	@endif
@endif
