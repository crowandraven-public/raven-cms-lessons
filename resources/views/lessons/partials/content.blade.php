<div class="marginbottom">
    @if (is_free($lesson) || Auth::user()->subscribed())
        {!! localize($lesson->content) !!}
    @else
        <p>{{ mb_strimwidth(strip_tags(localize($lesson->content)), 0, 500, "...") }}</p>
    @endif
</div>
