@if ($posts->count() > 0)
    @foreach($posts as $post)
        <div class="grid-item col-sm-6 col-md-4">
            <div class="card">
                <div class="card-bg">
                    <a href="/{{ CrowAndRaven\CMS\Models\Tag::getSlugById($post->type) }}/{{ localize($post->slug) }}">
                        <div class="image-bg" style="background-image: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('{{ isset($post->image) ? '/img/'.$post->image.'?w=600&h=400&fit=crop&q=60' : 'http://placehold.it/600x400' }}'); background-size: cover; background-repeat: no-repeat;">
                            <div class="tags">
                                @if ($post->created_at->gt(\Carbon\Carbon::now()->subDays(30)))
                                    <div class="card-new">
                                        <span class="card-new-content">New</span>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="title">
                            <h3>{{ mb_strimwidth(localize($post->title), 0, 150, "...") }}</h3>
                        </div>
                    </a>

                    <div class="actions push-right">
                        @if (is_subscribed_basic(Auth::user()))
                            @component('raven::partials.likeable', [
                                'likeable_action_url' => '/post/like/'.$post->id,
                                'post_type_name' => 'Post',
                                'liked' => (Auth::user()->likedPosts && Auth::user()->likedPosts->contains('id', $post->id)) ? '1': '0',
                                'liked_class' => 'fa fa-bookmark fa-fw pointer',
                                'not_liked_class' => 'fa fa-bookmark fa-fw pointer',
                                'tooltip_title_liked' => 'Saved',
                                'tooltip_title_not_liked' => 'Save'
                            ])
                            @endcomponent
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="col-sm-12">
        {{ $posts->links() }}
    </div>
@else
    <p>There are currently no posts to show.</p>
@endif
