@extends('raven::layouts.app-novue')

@section('title', localize($series->title))
@section('description', localize($series->excerpt))
@section('image', '/img/'.$series->image.'?w=1200&h=630&fit=crop', env('APP_URL').'/img/opengraph-1200x630.jpg')

@section('body-class', 'catalog page course')

@section('content')
<home :user="user" inline-template>
    <div class="catalog-nav">
        <div class="container">
            @include('raven::catalog.partials.nav')
        </div>
    </div>

    <div class="dark-band-video">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row no-gutter no-row-margin video-row">
                        <div class="col-md-12">
                            @if ($series->video)
                                <div class="video embed-responsive embed-responsive-16by9">
                                    <iframe id="video-main" class="embed-responsive-item" src="{{ $series->video }}" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                </div>
                            @else
                                <div class="photo-placeholder">
                                    <img src="{{ asset('/img/'.$series->image) }}" alt="{{ localize($series->title) }}" class="img-responsive" width="100%">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8 main-content">
                <h1>{{ localize($series->title) }}</h1>
                
                <div class="meta">
                    <a href="/series"><i class="fas fa-angle-left"></i> View All Series</a> &middot;
                    
                    {{--@include('raven::lessons.partials.tags')--}}
                
                    {{ ($lesson_count > 0) ? $lesson_count.' Lessons &middot; ' : '' }}

                    Run Time {{ ($length > 0) ? gmdate("i:s", $length) : gmdate("i:s", $series->length) }} &middot;

                    @if (Auth::user()->subscribed())
                        {{ round($percent_complete, 0) }}% Complete
                    @else
                        0% Complete
                    @endif
                </div>

                @include('raven::series.partials.content')

                @include('raven::series.partials.lessons')
            </div>
            <div class="col-md-4 sidebar">
                <div class="video-row-buttons marginbottom">
                    @if (is_free($series) || Auth::user()->subscribed())
                        @include('raven::series.partials.complete')
                        @include('raven::series.partials.save')
                    @else
                        @include('raven::series.partials.unlock')
                    @endif
                    
                    @include('raven::series.partials.share')
                </div>

                @include('raven::series.partials.recommended')
                
                @include('raven::series.partials.testimony')
                
                @include('raven::series.partials.author')
            </div>
        </div>
      
    	{{-- show related lessons, if they exist --}}
        @if (isset($related) && count($related) > 0)
        	<div class="row">
            	<div class="col-md-12">
    				<h3>You might also enjoy...</h3>
    			</div>
    			<div class="col-sm-12">
                	<h4 class="section-title">Related Lessons</h4>
            	</div>

            	@include('raven::lessons.partials.cards')

        	</div>
        @endif
    </div>
</home>
@endsection

@section('fscripts')
<script src="{{ elixir('js/plugins.js') }}"></script>
<script>
$(function(){
    var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        getSortData: {
            category: '[data-category]'
        }
    });
});
</script>
@endsection