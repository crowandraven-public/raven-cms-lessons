@extends('raven::layouts.app-novue')

@section('title', localize($lesson->title))
@section('description', localize($series->title).' - '.localize($lesson->title))
@section('image', $series->image, env('APP_URL').'/img/opengraph-1200x630.jpg')

@section('body-class', 'catalog page lesson')

@section('content')
<home :user="user" inline-template>
    <div class="catalog-nav">
        <div class="container">
            @include('raven::catalog.partials.nav')
        </div>
    </div>

    @if (is_free($lesson) || Auth::user()->subscribed())
        {{-- show lesson to subscribed user --}}
        <div class="dark-band-video">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row no-gutter no-row-margin">
                            <div class="col-md-12">
                                @if ($lesson->video)
                                    <div class="video embed-responsive embed-responsive-16by9">
                                        <iframe id="video-main" class="embed-responsive-item" src="{{ $lesson->video }}" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                    </div>
                                @else
                                    <div class="photo-placeholder">
                                        <img src="{{ asset('/img/'.$lesson->image) }}" alt="{{ localize($lesson->title) }}" class="img-responsive" width="100%">
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        {{-- show placeholder if not subscribed --}}
        <div class="dark-band-video">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row no-gutter no-row-margin video-row">
                            <div class="col-md-12">
                                <div class="photo-placeholder">
                                    <img src="{{ asset('/img/'.$lesson->image) }}" alt="{{ localize($lesson->title) }}" class="img-responsive" width="100%">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="container">
        <div class="row">
            <div class="col-md-8 main-content">
                <h1>{{ localize($lesson->title) }}</h1>
                
                <div class="meta">
                    <a href="/series/{{ localize($series->slug) }}"><i class="fas fa-angle-left"></i> Back to Series</a> &middot;
                    
                    {{--@include('raven::lessons.partials.tags')--}}
                
                    Run Time {{ gmdate("i:s", $lesson->length) }}
                </div>

                @include('raven::lessons.partials.content')

                @include('raven::series.partials.lessons')
            </div>
            <div class="col-md-4 sidebar">
                @if (is_free($lesson) || Auth::user()->subscribed())
                    @include('raven::series.partials.complete')

                    @if ($lesson->quiz)
                        @if ($user_completed == 1)
                            <div data-toggle="tooltip" data-placement="bottom" title="This module is complete. You can now proceed to the next module.">
                                <a href="#" class="btn btn-md btn-default btn-block disabled" disabled="disabled"><i class="fa fa-thumbs-o-up fa-fw pull-left"></i> Quiz Passed</a>
                            </div>
                        @else
                            <a href="#" class="btn btn-md btn-default btn-block marginbottom" data-toggle="modal" data-target="#quiz"><i class="fa fa-pencil fa-fw pull-left"></i> Take the Quiz</a>
                        @endif
                    @endif

                    @include('raven::lessons.partials.save')
                @else
                    <a href="/settings#/subscription"><i class="fas fa-lock fa-fw fa-2x" data-toggle="tooltip" data-placement="top" title data-original-title="Unlock this Lesson"></i></a>
                @endif
                    
                @include('raven::lessons.partials.share')

                @include('raven::lessons.partials.resources')

                @include('raven::lessons.partials.author')
            </div>
        </div>

  		{{-- show related lessons, if they exist --}}
        {{--@if (isset($lessons) && is_var_iterable($lessons))
        	<div class="row">
            	<div class="col-md-12">
					<h3>You might also enjoy...</h3>
				</div>
				<div class="col-sm-12">
                	<h4 class="section-title">Related Lessons</h4>
            	</div>

            	@include('lessons.partials.cards')

        	</div>
        @endif--}}

        {{-- show related services --}}
    </div>
</home>

@if ($lesson->quiz)
    <!-- Quiz -->
    <div class="modal fade" id="quiz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ $quiz->title }}</h4>
                </div>
                <div class="modal-body">
                    @include('raven::series.partials.quiz')
                </div>
            </div>
        </div>
    </div>
@endif
@endsection

@section('fscripts')
<script src="{{ elixir('js/plugins.js') }}"></script>


<script>
$(function(){
    var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        getSortData: {
            category: '[data-category]'
        }
    });
});
</script>

@endsection
