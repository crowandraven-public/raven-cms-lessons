<?php
if (!isset($lesson)) {
    // if there is no module
    $lesson = new stdClass();
    $lesson->id = null;
}
?>

@if (is_var_iterable($lessons))
    <div class="list-group">

        <?php
        $next = 1;
        ?>
                                        
        @foreach($lessons as $key => $less)
            @if (isset($config->certification))
                @if (Auth::check() && CrowAndRaven\CMS\Models\Lesson::isCompleted($less->id, $user_completed_lessons))
                    {{-- show module as completed --}}
                    <a class="list-group-item {{ ($less->id == $lesson->id) ? 'active' : '' }}" href="/series/{{ localize($series->slug) }}/{{ localize($less->slug) }}">
                        <div class="lesson-list">
                            
                            <div class="lesson-item">
                                @if (is_subscribed_basic(Auth::user()))
                                    <div data-likeable="1"
                                        data-likeable-disable-click="{{ ($less->quiz) ? '1' : '0' }}"
                                        data-liked="{{ (Auth::check() && CrowAndRaven\CMS\Models\Lesson::isCompleted($less->id, $user_completed_lessons)) ? '1' : '0' }}"
                                        data-likeable-action-url="/lesson/complete/{{$less->id}}"
                                        class="likeable likeable-type-completable">
                                        
                                        <span class="likeable-on off">
                                            <i class="fa fa-check fa-fw module-complete"></i>
                                        </span>

                                        <span class="likeable-off on">
                                            {{ $key + 1 }}
                                        </span>
                                    </div>
                                @else
                                    <span>{{ $key + 1 }}</span>
                                @endif
                            </div>
                            <div class="lesson-item">
                                <h3>{{ localize($less->title) }}</h3>
                                <p>{{ localize($less->excerpt) }}</p>
                            </div>
                            <div class="lesson-item">
                                <span class="badge">{{ gmdate("i:s", $less->length) }} min</span>
                            </div>
                        </div>
                    </a>

                    <?php
                    $next = $key + 2;
                    ?>
                @else
                    @if ($key + 1 == 1 || $key + 1 == $next)
                        {{-- show next lesson link if previous one completed --}}
                        <a class="list-group-item {{ ($less->id == $lesson->id) ? 'active' : '' }}" href="/series/{{ localize($series->slug) }}/{{ localize($less->slug) }}">
                            <div class="lesson-list">
                                <div class="lesson-item">
                                    @if (is_subscribed_basic(Auth::user()))
                                        <div data-likeable="1"
                                            data-likeable-disable-click="{{ ($less->quiz) ? '1' : '0' }}"
                                            data-liked="{{ (Auth::check() && CrowAndRaven\CMS\Models\Lesson::isCompleted($less->id, $user_completed_lessons)) ? '1' : '0' }}"
                                            data-likeable-action-url="/lesson/complete/{{$less->id}}"
                                            class="likeable likeable-type-completable">
                                            
                                            <span class="likeable-on off">
                                                <i class="fa fa-check fa-fw module-complete"></i>
                                            </span>

                                            <span class="likeable-off on">
                                                {{ $key + 1 }}
                                            </span>
                                        </div>
                                    @else
                                        <span>{{ $key + 1 }}</span>
                                    @endif
                                </div>
                                <div class="lesson-item">
                                    <h3>{{ localize($less->title) }}</h3>
                                    <p>{{ localize($less->excerpt) }}</p>
                                </div>
                                <div class="lesson-item">
                                    <span class="badge">{{ gmdate("i:s", $less->length) }} min</span>
                                </div>
                            </div>
                        </a>
                    @else
                        {{-- show module without link if previous one not completed --}}
                        <a class="list-group-item {{ ($less->id == $lesson->id) ? 'active' : '' }}">
                            <div class="lesson-list">
                                <div class="lesson-item">
                                    @if (is_subscribed_basic(Auth::user()))
                                        <div data-likeable="1"
                                            data-likeable-disable-click="{{ ($less->quiz) ? '1' : '0' }}"
                                            data-liked="{{ (Auth::check() && CrowAndRaven\CMS\Models\Lesson::isCompleted($less->id, $user_completed_lessons)) ? '1' : '0' }}"
                                            data-likeable-action-url="/lesson/complete/{{$less->id}}"
                                            class="likeable likeable-type-completable">
                                            
                                            <span class="likeable-on off">
                                                <i class="fa fa-check fa-fw module-complete"></i>
                                            </span>

                                            <span class="likeable-off on">
                                                {{ $key + 1 }}
                                            </span>
                                        </div>
                                    @else
                                        <span>{{ $key + 1 }}</span>
                                    @endif
                                </div>
                                <div class="lesson-item">
                                    <h3>{{ localize($less->title) }}</h3>
                                    <p>{{ localize($less->excerpt) }}</p>
                                </div>
                                <div class="lesson-item">
                                    <span class="badge">{{ gmdate("i:s", $less->length) }} min</span>
                                </div>
                            </div>
                        </a>
                    @endif
                @endif
            @else
                <a class="list-group-item {{ ($less->id == $lesson->id) ? 'active' : '' }}" href="/series/{{ localize($series->slug) }}/{{ localize($less->slug) }}">
                    <div class="lesson-list">
                        <div class="lesson-item">
                            @if (is_subscribed_basic(Auth::user()))
                                <div data-likeable="1"
                                    data-likeable-disable-click="{{ ($less->quiz) ? '1' : '0' }}"
                                    data-liked="{{ (Auth::check() && CrowAndRaven\CMS\Models\Lesson::isCompleted($less->id, $user_completed_lessons)) ? '1' : '0' }}"
                                    data-likeable-action-url="/lesson/complete/{{$less->id}}"
                                    class="likeable likeable-type-completable">
                                    
                                    <span class="likeable-on off">
                                        <i class="fa fa-check fa-fw module-complete"></i>
                                    </span>

                                    <span class="likeable-off on">
                                        {{ $key + 1 }}
                                    </span>
                                </div>
                            @else
                                <span>{{ $key + 1 }}</span>
                            @endif

                        </div>
                        <div class="lesson-item">
                            <h3>{{ localize($less->title) }}</h3>
                            <p>{{ localize($less->excerpt) }}</p>
                        </div>
                        <div class="lesson-item">
                            <span class="badge">{{ gmdate("i:s", $less->length) }} min</span>
                        </div>
                    </div>
                </a>
            @endif

            <?php
            $next = $next++;
            ?>
        @endforeach

        {{-- download certificate of completion --}}
        @if (isset($series->config))
            @if (Auth::check() && $percent_complete === 100)
                <a id="hidden-download-link" class="list-group-item" href="/series/{{ $series->id }}/certificate">
                    <i class="fa fa-download fa-fw module-complete"></i> Series Complete - Download Certificate of Completion
                </a>

                <div style="display: none;" id="shown-but-not-complete" data-toggle="tooltip" data-placement="bottom" title="Upon finishing this series, you can download a certificate.">
                    <a href="#" class="list-group-item disabled" disabled="disabled"><i class="fa fa-download fa-fw"></i> Download Certificate of Completion</a>
                </div>
            @else
                <div id="shown-but-not-complete" data-toggle="tooltip" data-placement="bottom" title="Upon finishing this series, you can download a certificate.">
                    <a href="#" class="list-group-item disabled" disabled="disabled"><i class="fa fa-download fa-fw"></i> Download Certificate of Completion</a>
                </div>

                <a style="display: none;" id="hidden-download-link" class="list-group-item" href="/series/{{ $series->id }}/certificate">
                    <i class="fa fa-download fa-fw module-complete"></i> Series Complete - Download Certificate of Completion
                </a>
            @endif
        @endif
    </div>
@endif