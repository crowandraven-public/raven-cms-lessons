@if (localize($series->recommended_for))
    <div class="marginbottom">
        <h3 class="section-title">Recommended For</h3>
        {!! localize($series->recommended_for) !!}
    </div>
@endif
