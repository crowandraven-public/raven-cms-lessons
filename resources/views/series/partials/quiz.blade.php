{!! Form::open(['url' => 'series/quiz/'.$quiz->id.'/grade', 'role' => 'form']) !!}
    
    <div class="row">
        <div class="col-md-12">
        	{!! $quiz->content !!}
        </div>
    </div>
    
    <!-- questions/answers -->
    @foreach ($quiz->questions as $question_index => $question)
        <?php $question_index+=1; ?>
	    <div class="row form-group">
	    	{!! Form::label('question_'.$question_index, $question_index.') '.$question->questionText, ['class' => 'col-md-12 control-label required']) !!}
	        <div class="col-md-12 marginleft-sm">
                @foreach ($question->answers as $answer)
	            	{!! Form::radio('answer_'.$question_index, $answer->itemValue, $answer->itemCheckboxState) !!} {{ $answer->itemValue }}<br>
	            @endforeach
	        </div>
	    </div>
    @endforeach

    <!-- Submit Button -->
    <div class="row form-group">
        <div class="col-md-12">
            <input type="hidden" name="series" value="{{ $series->id }}">
            <input type="hidden" name="lesson" value="{{ $lesson->id }}">
            <input type="hidden" name="order" value="{{ $lesson->order }}">
            @if ($next)
                <input type="hidden" name="next" value="{{ $next->slug }}">
            @endif
            {!! Form::submit('Submit', ['class' => 'btn btn-primary btn-md margintop-sm']) !!}
        </div>
    </div>
{!! Form::close() !!}
