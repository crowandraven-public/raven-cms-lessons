@if ($series->testimony_text)
    <div class="marginbottom">
        <h3 class="section-title">What Subscribers Are Saying</h3>
        <blockquote>
            <p><em>"{{ $series->testimony_text }}"</em></p>
            <p class="small">{{ $series->testimony_attrib }}</p>
        </blockquote>
    </div>
@endif
