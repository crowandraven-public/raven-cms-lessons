<div class="marginbottom">
    @if (is_free($series) || Auth::user()->subscribed())
        {!! localize($series->content) !!}
    @else
        <p>{{ mb_strimwidth(strip_tags(localize($series->content)), 0, 500, "...") }}</p>
    @endif
</div>
