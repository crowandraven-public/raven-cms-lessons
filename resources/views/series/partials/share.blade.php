<span class="fa-2x">
    <a href="https://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}&t={{ localize($series->title) }}" target="_blank" title="Share on Facebook" class="plain"><i class="fab fa-facebook-square fa-fw"></i></a>

    <a href="https://twitter.com/intent/tweet?source={{ Request::url() }}&text={{ localize($series->title) }}:{{ env('APP_NAME') }}&via={{ Config::get('site.social.twitter') }}" target="_blank" title="Tweet" class="plain"><i class="fab fa-twitter-square fa-fw"></i></a>

    <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url={{ Request::url() }}&amp;title={{ localize($series->title) }}&amp;summary={{ localize($series->excerpt) }}" target="_blank" class="plain"><i class="fab fa-linkedin fa-fw"></i></a>
</span>