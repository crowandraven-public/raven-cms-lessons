@if (Auth::user()->subscribed())
	<div class="inline fa-2x">
		@component('raven::partials.likeable', [
			'likeable_action_url' => '/series/like/'.$series->id,
			'post_type_name' => 'Series',
			'liked' => (Auth::user()->likedSeries && Auth::user()->likedSeries->contains('id', $series->id)) ? '1': '0',
			'liked_class' => 'fas fa-bookmark fa-fw saved pointer',
			'not_liked_class' => 'fas fa-bookmark fa-fw pointer',
			'tooltip_title_liked' => 'Series Saved',
			'tooltip_title_not_liked' => 'Save this Series'
		])
		@endcomponent
	</div>
@endif
