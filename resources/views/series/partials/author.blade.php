@if (isset($author->bio))
	<div class="marginbottom">
        <h3 class="sidebar section-title">Your Coach</h3>
        <p>
            <img src="{{ $author->photo_url }}" class="coach-thumbnail" alt="{{ $author->name }}">
            <strong>{{ $author->name }}</strong> {{ $author->bio }}
        </p>
    </div>
@endif
