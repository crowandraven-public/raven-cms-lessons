<div class="inline fa-2x">
    <div id="series-complete-indicator"
        class="likeable likeable-type-completeable">
    
        <span class="likeable-on {{ ($percent_complete == 100) ? 'on': 'off' }}" data-toggle="tooltip" data-placement="top" title data-original-title="Series Completed">
            <i class="far fa-check-circle fa-fw completed pointer"></i>
        </span>

        <span class="likeable-off {{ ($percent_complete == 100) ? 'off': 'on' }}" data-toggle="tooltip" data-placement="top" title data-original-title="Series not Completed">
            <i class="far fa-check-circle fa-fw pointer"></i>
        </span>

    </div>
</div>
