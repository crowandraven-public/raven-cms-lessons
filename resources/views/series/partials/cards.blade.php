@if ($seriess->count() > 0)
    <div class="col-sm-12">
        <div id="grid" class="grid lessons-grid" style="margin-left: -1.5rem; margin-right: -1.5rem;">
            <div class="grid-sizer col-sm-6 col-md-4"></div>

            @foreach($seriess as $series)
                @php
                    $tags = '';
                    if (isset($series->tags)) {
                        foreach ($series->tags as $tag) {
                            $tags .= localize($tag->slug).' ';
                        }
                        $tags = trim($tags);
                    }

                    $config = $series->config;
                @endphp

                <div class="grid-item col-sm-6 col-md-4 all {{ $tags }}">
                    
                    <div class="card">
                        <div class="card-bg">
                            <a href="/series/{{ localize($series->slug) }}">
                                <div class="image-bg" style="background-image: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('{{ isset($series->image) ? '/img/'.$series->image.'?w=600&h=400&fit=crop&q=60' : 'http://placehold.it/600x400' }}'); background-size: cover; background-repeat: no-repeat;">
                                    <div class="tags">
                                        @if (is_free($series))
                                            <div class="card-free">
                                                <span class="card-free-content">Free</span>
                                            </div>
                                        @else
                                            @if (!is_subscribed_basic(Auth::user()))
                                                <span data-toggle="tooltip" data-placement="top" title="Upgrade to Unlock"><i class="fa fas fa-lock fa-fw"></i></span>
                                            @endif
                                        @endif

                                        @if ($series->created_at->gt(\Carbon\Carbon::now()->subDays(30)))
                                            <div class="card-new">
                                                <span class="card-new-content">New</span>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="title">
                                    <h3>{{ mb_strimwidth(localize($series->title), 0, 150, "...") }}</h3>
                                </div>
                            </a>

                            <div class="actions push-right">
                            @if (Auth::user()->subscribed())
                                <div class="likeable series-indicator likeable-type-completable {{ (Auth::user()->completedSeries && Auth::user()->completedSeries->contains('id', $series->id)) ? 'disabled-liked': '' }}">
                                    <span data-toggle="tooltip"
                                        data-placement="top"
                                        title="{{ (Auth::user()->completedSeries && Auth::user()->completedSeries->contains('id', $series->id)) ? 'Series Completed': 'Series not completed' }}">
                                        <i class="fa fa-check fa-fw pointer"></i>
                                    </span>
                                </div>
                                
                                @component('raven::partials.likeable', [
                                    'likeable_action_url' => '/series/like/'.$series->id,
                                    'post_type_name' => 'Series',
                                    'liked' => (Auth::user()->likedSeries && Auth::user()->likedSeries->contains('id', $series->id)) ? '1': '0',
                                    'liked_class' => 'fa fa-bookmark fa-fw pointer',
                                    'not_liked_class' => 'fa fa-bookmark fa-fw pointer',
                                    'tooltip_title_liked' => 'Series Saved',
                                    'tooltip_title_not_liked' => 'Save Series'
                                ])
                                @endcomponent
                            @endif

                            </div>
                        </div>
                    </div>
                </div>

            @endforeach

        </div>
    </div>

    @if (method_exists($seriess, 'links'))
        <div class="col-sm-12">
            {{ $seriess->links() }}
        </div>
    @endif
@else
    <p>There are currently no series available.</p>
@endif
