<?php

namespace CrowAndRaven\CMS\Console\Commands;

use CrowAndRaven\CMS\Console\Command;
use CrowAndRaven\CMS\Models\LessonsUser;

class InstallLessons extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'raven:install-lessons';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the e-learning package on your Raven CMS instance.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line("Initializing installation script.", 'green');

        shell_exec('php artisan vendor:publish --provider="CrowAndRaven\CMS\LessonsServiceProvider" --tag=migrations');
        $this->line("The lessons database migrations have been published.", 'green');
        
        shell_exec('composer dump-autoload');
        
        shell_exec('php artisan migrate');
        $this->line("The lessons migrations have been run.", 'green');

        $this->line("Wohoo! The install is complete.", 'green');
    }
}
