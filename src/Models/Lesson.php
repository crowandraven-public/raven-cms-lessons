<?php

namespace CrowAndRaven\CMS\Models;

use Auth;
use CrowAndRaven\CMS\Models\Like;
use CrowAndRaven\CMS\Models\Tag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Lesson extends Model
{
    
    use \CrowAndRaven\CMS\Traits\AttachablesTrait;
    
    protected $fillable = [
        'status',
        'title',
        'slug',
        'excerpt',
        'content',
        'author',
        'image',
        'file',
        'video',
        'length',
        'config',
        'is_private',
        'is_featured',
        'created_by',
        'updated_by'
    ];

    public function lessonImage()
    {
        return $this->hasOne('CrowAndRaven\CMS\Models\Attachment', 'id', 'image');
    }

    public function lessonFile()
    {
        return $this->hasOne('CrowAndRaven\CMS\Models\Attachment', 'id', 'file');
    }

    public function tags()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\Tag', 'taggable');
    }

    public function resources()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\Resource', 'resourcable');
    }

    public function series()
    {
        return $this->belongsToMany('CrowAndRaven\CMS\Models\Series');
    }

    public function quiz()
    {
        return $this->hasOne('CrowAndRaven\CMS\Models\Quiz');
    }

    public function author()
    {
        return $this->belongsTo('CrowAndRaven\CMS\Models\User');
    }

    public function files()
    {
        return $this->hasMany('CrowAndRaven\CMS\Models\Media');
    }

    public function likes()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\User', 'likeable')->whereDeletedAt(null);
    }

    public function completes()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\User', 'completable')->whereDeletedAt(null);
    }

    public function getIsLikedAttribute()
    {
        $like = $this->likes()->whereUserId(Auth::id())->first();
        return (!is_null($like)) ? true : false;
    }

    public static function create($request)
    {
        $translatable = ['title', 'content']; // these must be of type JSON

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($request as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        // create unique slug
        foreach ($request as $key => $value) {
            if (strpos($key, 'title-') === 0) {
                $loc = substr($key, strpos($key, "-") + 1);
                $fieldset[$loc] = self::uniqueSlug(Str::slug($value, '-'), $loc, false);
            }
        }
        $slug = json_encode($fieldset);

        // create excerpt
        foreach ($request as $key => $value) {
            if (strpos($key, 'content-') === 0) {
                $fieldset[$loc] = strip_tags(substr($value, 0, 200));
            }
        }
        $excerpt = json_encode($fieldset);

        $image = (isset($request['image']) && $request['image'] != '0') ? $request['image'] : null;
        $file = (isset($request['file']) && $request['file'] != '0') ? $request['file'] : null;
        $video = (isset($request['video']) && $request['video'] != '0'  && trim($request['video']) != '') ? $request['video'] : null;
        $quiz = (isset($request['quiz']) && $request['quiz'] != '0') ? $request['quiz'] : null;
        $is_featured = (isset($request['is_featured']) ? $request['is_featured'] : null);
        $is_private = (isset($request['is_private']) ? $request['is_private'] : null);
        $author = (isset($request['author']) ? $request['author'] : null);
        $length = ((isset($request['length']) && is_numeric($request['length'])) ? $request['length'] : null);
        $config = (isset($request['config']) ? $request['config'] : '{}');

        // save new lesson
        $lesson = new Lesson;
        $lesson->status = $request['status'];
        $lesson->title = $title;
        $lesson->slug = $slug;
        $lesson->content = $content;
        $lesson->excerpt = $excerpt;
        $lesson->author = $author;
        $lesson->image = $image;
        $lesson->file = $file;
        $lesson->video = $video;
        $lesson->length = $length;
        $lesson->config = $config;
        $lesson->quiz = $quiz;
        $lesson->is_featured = $is_featured;
        $lesson->is_private = $is_private;
        $lesson->created_by = Auth::user()->id;
        $lesson->updated_by = Auth::user()->id;
        $lesson->save();
        if (isset($request['addbysearch_attachments'])) {
            $lesson->updateAttachments($request['addbysearch_attachments']);
        }

        return $lesson;
    }

    public static function updateLesson($request, $id)
    {
        $translatable = ['title', 'excerpt', 'content']; // these must be of type JSON
        
       
        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($request as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        // check unique slug
        foreach ($request as $key => $value) {
            if (strpos($key, 'slug-') === 0) {
                $loc = substr($key, strpos($key, "-") + 1);
                $fieldset[$loc] = self::uniqueSlug(Str::slug($value, '-'), $loc, true);
            }
        }
        $slug = json_encode($fieldset);

        $image = (isset($request['image']) && $request['image'] != '0') ? $request['image'] : null;
        $file = (isset($request['file']) && $request['file'] != '0') ? $request['file'] : null;
        $video = (isset($request['video']) && $request['video'] != '0'  && trim($request['video']) != '') ? $request['video'] : null;
        $quiz = (isset($request['quiz']) && $request['quiz'] != '0') ? $request['quiz'] : null;
        $is_featured = (isset($request['is_featured']) ? $request['is_featured'] : null);
        $is_private = (isset($request['is_private']) ? $request['is_private'] : null);
        $author = (isset($request['author']) ? $request['author'] : null);
        $length = ((isset($request['length']) && is_numeric($request['length'])) ? $request['length'] : null);
        $config = (isset($request['config']) ? $request['config'] : '{}');

        $lesson = Lesson::findOrFail($id);
        $lesson->status = $request['status'];
        $lesson->title = $title;
        $lesson->slug = $slug;
        $lesson->content = $content;
        $lesson->excerpt = $excerpt;
        $lesson->author = $author;
        $lesson->image = $image;
        $lesson->file = $file;
        $lesson->video = $video;
        $lesson->length = $length;
        $lesson->config = $config;
        $lesson->quiz = $quiz;
        $lesson->is_featured = $is_featured;
        $lesson->is_private = $is_private;
        $lesson->updated_by = Auth::user()->id;

        if (isset($request['addbysearch_attachments'])) {
            $lesson->updateAttachments($request['addbysearch_attachments']);
        }
        
        $lesson->save();

        return $lesson;
    }

    /**
     * Create a unique slug name
     * @param  string  $slug   The slug name to check if unique
     * @param  string  $locale What is the locale passed
     * @param  boolean $update Are we updating a post (true) or is this a new one (false)?
     * @return string          The updated slug
     */
    public static function uniqueSlug($slug, $locale = null, $update = false)
    {
        $locale = $locale ?? app()->getLocale();
        $result = Lesson::where("slug->{$locale}", $slug)->get();

        if ($update) {
            if (is_array($result) && count($result) > 1) {
                $slug = $slug.'-'.rand(1, 100);
            }
        } else {
            if (is_array($result) && count($result) > 0) {
                $slug = $slug.'-'.rand(1, 100);
            }
        }

        return $slug;
    }

    /**
     * Create a new (or update an) excerpt for the lesson. Pulls the first $size characters
     * @param  object  $data Lesson data which includes the lesson content
     * @param  integer $size Number of characters to grab for the excerpt
     * @return string        The new (or updated) excerpt
     */
    public static function getExcerpt($data, $size = 200)
    {
        $excerpt = (isset($data->excerpt) ? strip_tags(substr($data->excerpt, 0, 200)) : strip_tags(substr($data->content, 0, 200)));

        return $excerpt;
    }

    /**
     * Disconnect likes with user
     */
    public function unlike($user_id, $likeable_id)
    {
        $user = User::find($user_id);
        $like = Like::where('likeable_id', $likeable_id)
            ->where('user_id', $user_id)->first();
        if ($like) {
            return $like->delete();
        }
        return false;

    }

    public static function getLessonBySlug($slug)
    {
        $locale = $locale ?? app()->getLocale();

        $lesson = Lesson::where("slug->{$locale}", $slug)->firstOrFail();

        return $lesson;
    }

    public static function getLessons($type_array = null, $track = null)
    {
        $locale = $locale ?? app()->getLocale();

        if (is_array($type_array)) {
            $lessons = Lesson::where('status', 1)->whereIn('config->type', $type_array);
        } else {
            $lessons = Lesson::where('status', 1);
        }

        if (isset($track)) {
            $lessons->whereHas('tags', function ($query) use ($locale, $track) {
                $query->where("slug->{$locale}", $track);
            });
        }

        $lessons->orderBy('created_at', 'desc');

        return $lessons;
    }

    public static function getAllLessons()
    {
        $locale = $locale ?? app()->getLocale();
        $lessons = Lesson::where('status', 1)->orderBy("title->{$locale}")->get();

        $lessons = $lessons->map(function ($l) use ($locale) {
            $title = localize($l->title, $locale);
            $l->title = $title;
            return $l;
        });

        return $lessons->pluck('title', 'id');
    }

    public static function getRelatedLessonsByTag($tag)
    {
        $related = Tag::find($tag)->lessons;
        $related_count = is_var_iterable($related) ? count($related) : 0;
        $count = ($related_count >= 3) ? 3 : $related_count;
        if ($count == 3) {
            $lessons = Tag::find($first_tag->id)->lessons->random($count);
        } else {
            $lessons = Tag::find($first_tag->id)->lessons;
        }

        return $lessons;
    }

    /**
     * Used in lessons list to check which ones have been completed
     */
    public static function isCompleted($current_lesson_id, $user_completed_lessons)
    {
        $is_completed = false;

        if (in_array($current_lesson_id, $user_completed_lessons)) {
            $is_completed = true;
        }

        return $is_completed;
    }



    public static function getNextLesson($series_id, $current_lesson_order)
    {
        $series = Series::find($series_id);
        if ($current_lesson_order == 0) {
            $current_lesson_order = 1;
        } else {
            $current_lesson_order+=1;
        }
        $next = $series->lessons()->where('order', $current_lesson_order)->get();
      
        if ($next) {
            return $next;
        }
        
        return null;
    }
}
