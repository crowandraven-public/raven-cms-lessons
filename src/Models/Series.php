<?php

namespace CrowAndRaven\CMS\Models;

use Auth;
use CrowAndRaven\CMS\Models\Tag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Series extends Model
{
    use \CrowAndRaven\CMS\Traits\AttachablesTrait;
    
    protected $fillable = [
        'status',
        'title',
        'slug',
        'excerpt',
        'recommended_for',
        'content',
        'testimony_text',
        'testimony_attrib',
        'author',
        'image',
        'video',
        'config',
        'is_private',
        'is_featured',
        'created_by',
        'updated_by'
    ];

    public function seriesImage()
    {
        return $this->hasOne('CrowAndRaven\CMS\Models\Attachment', 'id', 'image');
    }

    public function tags()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\Tag', 'taggable');
    }

    public function resources()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\Resource', 'resourcable');
    }

    public function lessons()
    {
        return $this->belongsToMany('CrowAndRaven\CMS\Models\Lesson')->withPivot('order');
    }

    public function author()
    {
        return $this->belongsTo('CrowAndRaven\CMS\Models\User');
    }

    public function teams()
    {
        return $this->belongsToMany('App\Team', 'series_team');
    }

    /* not sure about this -- maybe a better way?*/
    public function team_series()
    {
        return $this->belongsToMany('App\Team', 'series_team');
    }

    public function likes()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\User', 'likeable')->whereDeletedAt(null);
    }

    public function completes()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\User', 'completeable')->whereDeletedAt(null);
    }

    public function enrolls()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\User', 'enrollable')->whereDeletedAt(null);
    }

    public function getIsLikedAttribute()
    {
        $like = $this->likes()->whereUserId(Auth::id())->first();
        return (!is_null($like)) ? true : false;
    }

    public static function create($request)
    {
        $translatable = ['title', 'recommended_for', 'content']; // these must be of type JSON

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($request as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        // create unique slug
        foreach ($request as $key => $value) {
            if (strpos($key, 'title-') === 0) {
                $loc = substr($key, strpos($key, "-") + 1);
                $fieldset[$loc] = self::uniqueSlug(Str::slug($value, '-'), $loc, false);
            }
        }
        $slug = json_encode($fieldset);

        // create excerpt
        foreach ($request as $key => $value) {
            if (strpos($key, 'content-') === 0) {
                $fieldset[$loc] = strip_tags(substr($value, 0, 200));
            }
        }
        $excerpt = json_encode($fieldset);

        $image = (isset($request['image']) && $request['image'] != '0') ? $request['image'] : null;
        $video = (isset($request['video']) && $request['video'] != '0' && trim($request['video']) != '') ? $request['video'] : null;
        $is_featured = (isset($request['is_featured']) ? $request['is_featured'] : null);
        $is_private = (isset($request['is_private']) ? $request['is_private'] : null);
        $testimony_text = (isset($request['$testimony_text']) && $request['$testimony_text'] != '') ? $request['$testimony_text'] : null;
        $testimony_attrib = (isset($request['$testimony_attrib']) && $request['$testimony_attrib'] != '') ? $request['$testimony_attrib'] : null;
        $author = (isset($request['author']) ? $request['author'] : null);
        $config = (isset($request['config']) ? $request['config'] : '{}');

        // get or set user id
        if (isset($request['updated_by'])) {
            $created_by = $request['updated_by'];
        } else {
            $created_by = Auth::user()->id;
        }

        // save new series
        $series = new Series;
        $series->status = $request['status'];
        $series->title = $title;
        $series->slug = $slug;
        $series->content = $content;
        $series->excerpt = $excerpt;
        $series->recommended_for = $recommended_for;
        $series->testimony_text = $testimony_text;
        $series->testimony_attrib = $testimony_attrib;
        $series->author = $author;
        $series->image = $image;
        $series->video = $video;
        $series->config = $config;
        $series->is_featured = $is_featured;
        $series->is_private = $is_private;
        $series->created_by = $created_by;
        $series->updated_by = $created_by;
        $series->save();

        return $series;
    }

    public static function updateSeries($request, $id)
    {
        $translatable = ['title', 'excerpt', 'content', 'recommended_for']; // these must be of type JSON

        // iterate through translatable fields
        foreach ($translatable as $field) {
            $fieldset = [];
            foreach ($request as $key => $value) {
                if (strpos($key, $field.'-') === 0) {
                    $loc = substr($key, strpos($key, "-") + 1);
                    $fieldset[$loc] = $value;
                }
            }
            $$field = json_encode($fieldset);
        }

        // check unique slug
        foreach ($request as $key => $value) {
            if (strpos($key, 'slug-') === 0) {
                $loc = substr($key, strpos($key, "-") + 1);
                $fieldset[$loc] = self::uniqueSlug(Str::slug($value, '-'), $loc, true);
            }
        }
        $slug = json_encode($fieldset);

        $image = (isset($request['image']) && $request['image'] != '0') ? $request['image'] : null;
        $video = (isset($request['video']) && $request['video'] != '0' && trim($request['video']) != '') ? $request['video'] : null;
        $is_featured = (isset($request['is_featured']) ? $request['is_featured'] : null);
        $is_private = (isset($request['is_private']) ? $request['is_private'] : null);
        $testimony_text = (isset($request['$testimony_text']) && $request['$testimony_text'] != '') ? $request['$testimony_text'] : null;
        $testimony_attrib = (isset($request['$testimony_attrib']) && $request['$testimony_attrib'] != '') ? $request['$testimony_attrib'] : null;
        $author = (isset($request['author']) ? $request['author'] : null);
        $config = (isset($request['config']) ? $request['config'] : '{}');

        // get or set user id
        if (isset($request['updated_by'])) {
            $updated_by = $request['updated_by'];
        } else {
            $updated_by = Auth::user()->id;
        }

        $series = Series::findOrFail($id);
        $series->status = $request['status'];
        $series->title = $title;
        $series->slug = $slug;
        $series->content = $content;
        $series->recommended_for = $recommended_for;
        $series->excerpt = $excerpt;
        $series->testimony_text = $testimony_text;
        $series->testimony_attrib = $testimony_attrib;
        $series->author = $author;
        $series->image = $image;
        $series->video = $video;
        $series->config = $config;
        $series->is_featured = $is_featured;
        $series->is_private = $is_private;
        $series->updated_by = $updated_by;
        $series->save();

        return $series;
    }

    /**
     * Create a unique slug name
     * @param  string  $slug   The slug name to check if unique
     * @param  string  $locale What is the locale passed
     * @param  boolean $update Are we updating a post (true) or is this a new one (false)?
     * @return string          The updated slug
     */
    public static function uniqueSlug($slug, $locale = null, $update = false)
    {
        $locale = $locale ?? app()->getLocale();
        $result = Series::where("slug->{$locale}", $slug)->get();

        if ($update) {
            if (is_array($result) && count($result) > 1) {
                $slug = $slug.'-'.rand(1, 100);
            }
        } else {
            if (is_array($result) && count($result) > 0) {
                $slug = $slug.'-'.rand(1, 100);
            }
        }

        return $slug;
    }

    /**
     * Create a new (or update an) excerpt for the series. Pulls the first $size characters
     * @param  object  $data Lesson data which includes the lesson content
     * @param  integer $size Number of characters to grab for the excerpt
     * @return string        The new (or updated) excerpt
     */
    public static function getExcerpt($data, $size = 200)
    {
        $excerpt = (isset($data->excerpt) ? strip_tags(substr($data->excerpt, 0, 200)) : strip_tags(substr($data->content, 0, 200)));

        return $excerpt;
    }

    public static function getRelatedSeriesByTag($tag)
    {
        $related = Tag::find($tag)->series;
        $related_count = is_var_iterable($related) ? count($related) : 0;
        $count = ($related_count >= 3) ? 3 : $related_count;
        if ($count == 3) {
            $series = Tag::find($first_tag->id)->series->random($count);
        } else {
            $series = Tag::find($first_tag->id)->series;
        }

        return $series;
    }

    public static function getSeries($type_array = null, $track = null)
    {
        $locale = $locale ?? app()->getLocale();

        if (is_array($type_array)) {
            $series = Series::where('status', 1)->whereIn('config->type', $type_array);
        } else {
            $series = Series::where('status', 1);
        }

        if (isset($track)) {
            $series->whereHas('tags', function ($query) use ($locale, $track) {
                $query->where("slug->{$locale}", $track);
            });
        }

        $series->orderBy('created_at', 'desc');

        return $series;
    }

    /**
     * Get the next lesson in a series
     * @param  object $lessons
     * @param  object $current
     */
    public static function getNext($lessons, $current)
    {
        $slugs = $lessons->pluck('slug')->toArray();
        $current_position = array_search($current->slug, $slugs);
        $next_position = $slugs[$current_position + 1];

        if (isset($next_position)) {
            $next = Lesson::getLessonBySlug(localize($next_position));
        } else {
            $next = null;
        }

        return $next;
    }

    /**
     * Query scope to get only public series
     * @param  object $query an existing query
     */
    public function scopePublic($query)
    {
        $query->where('is_private', null);

        return $query;
    }

    /**
     * Query scope to get only private series
     * @param  object $query an existing query
     */
    public function scopePrivate($query, $team = null)
    {
        if (isset($team)) {
            $query->whereHas('team_series', function ($teamquery) use ($team) {
                $teamquery->where('team_id', $team);
            });
        } else {
            $query->where('is_private', 1);
        }

        return $query;
    }

    /**
     * Query scope to get only private series
     * @param  object $query an existing query
     */
    public function scopeCustom($query, $team = null)
    {
        if (isset($team)) {
            $query->whereHas('team_series', function ($teamquery) use ($team) {
                $teamquery->where('team_id', $team);
            });

            $query->orWhere('is_private', null);
        } else {
            $query->where('is_private', 1);
        }

        return $query;
    }

    /**
     * Query scope to get the latest series
     * @param  object $query an existing query
     */
    public function scopeLatest($query)
    {
        return $query->orderBy('created_at', 'desc')->first();
    }

    /**
     * Query scope to get all featured series
     * @param  object $query an existing query
     * @param  integer $take the number of series to get
     */
    public function scopeFeatured($query, $take = null)
    {
        $query->where('is_featured', 1)->orderBy('created_at', 'desc');

        if (isset($take)) {
            $query->take($take);
        }

        return $query;
    }

    /**
     * Query scope to get the series pairs for forms
     * @param  object $query an existing query
     */
    public function scopePairs($query)
    {
        $locale = $locale ?? app()->getLocale();

        $series = $query->orderBy("title->{$locale}")->get();

        $series = $series->map(function ($l) use ($locale) {
            $title = localize($l->title, $locale);
            $l->title = $title;
            return $l;
        });

        return $series->pluck('title', 'id');
    }

    public static function getSeriesBySlug($slug)
    {
        $locale = $locale ?? app()->getLocale();

        $series = Series::where("slug->{$locale}", $slug)->firstOrFail();

        return $series;
    }

    /**
     * Check to see if series has been completed
     */
    public static function isCompleted($count_lessons, $count_completed_lessons)
    {
        $is_completed = false;

        if ($count_lessons == $count_completed_lessons) {
            $is_completed = true;
        }

        return $is_completed;
    }
}
