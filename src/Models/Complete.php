<?php

namespace CrowAndRaven\CMS\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Complete extends Model
{
    use SoftDeletes;

    protected $table = 'completeables';
    protected $fillable = ['completeable_id', 'completeable_type', 'user_id'];

    public function lessons()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Lesson', 'completeable');
    }

    public function series()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Series', 'completeable');
    }

    public function quizzes()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Quiz', 'completeable');
    }
}
