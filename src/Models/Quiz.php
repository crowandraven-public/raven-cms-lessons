<?php

namespace CrowAndRaven\CMS\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $fillable = [
        'status',
        'title',
        'slug',
        'content',
        'questions',
        'locale',
        'created_by',
        'updated_by'
    ];


    public function getQuestionsAttribute($value)
    {   
        if(is_array($this->config)) {
            $config = $this->config;
        } else {
            $config = json_decode($this->config);
        }
        return $config->questions;
    }

    public function tags()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\Tag', 'taggable');
    }

    public function lessons()
    {
        return $this->belongsToMany('CrowAndRaven\CMS\Models\Lesson');
    }

    public function completes()
    {
        return $this->morphToMany('CrowAndRaven\CMS\Models\User', 'completable')->whereDeletedAt(null);
    }

    public static function create($request)
    {
        $slug = self::uniqueSlug(str_slug($request['title'], '-'), false);
        $locale = $request['locale'] ?? app()->getLocale();

        $quiz = new Quiz;
        $quiz->status = $request['status'];
        $quiz->title = $request['title'];
        $quiz->slug = $slug;
        $quiz->content = $request['content'];
        $quiz->config = $request['config'];
        $quiz->locale = $request['locale'];
        $quiz->created_by = Auth::user()->id;
        $quiz->updated_by = Auth::user()->id;
        $quiz->save();

        return $quiz;
    }

    public static function updateQuiz($request, $id)
    {
        $slug = self::uniqueSlug(str_slug($request['slug'], '-'), true);
        $locale = $request['locale'] ?? app()->getLocale();
        $quiz = Quiz::findOrFail($id);
        $quiz->status = $request['status'];
        $quiz->title = $request['title'];
        $quiz->slug = $slug;
        $quiz->content = $request['content'];
        $quiz->config = $request['config'];
        $quiz->updated_by = Auth::user()->id;
        $quiz->save();

        return $quiz;
    }

    /**
     * Create a unique slug name
     * @param  string  $slug   The slug name to check if unique
     * @param  boolean $update Are we updating a post (true) or is this a new one (false)?
     * @return string          The updated slug
     */
    public static function uniqueSlug($slug, $update = false)
    {
        $result = Quiz::where('slug', $slug)->get();

        if ($update) {
            if (is_array($result) && count($result) > 1) {
                $slug = $slug.'-'.rand(1, 100);
            }
        } else {
            if (is_array($result) && count($result) > 0) {
                $slug = $slug.'-'.rand(1, 100);
            }
        }

        return $slug;
    }

    public static function getQuizBySlug($slug)
    {
        $quiz = Quiz::where('status', 1)->where('slug', $slug)->firstOrFail();

        return $quiz;
    }

    public static function getQuizById($id)
    {
        $quiz = Quiz::where('status', 1)->where('id', $id)->firstOrFail();

        return $quiz;
    }

    public static function getQuizzes()
    {
        $quizzes = Quiz::where('status', 1)->orderBy('created_at', 'desc');

        return $quizzes;
    }

    public static function getAllQuizzes()
    {
        $quizzes = Quiz::where('status', 1)->orderBy('title', 'asc')->pluck('title', 'id');

        return $quizzes;
    }
}
