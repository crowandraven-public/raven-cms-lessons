<?php

namespace CrowAndRaven\CMS\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enroll extends Model
{
    public function lessons()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Lesson', 'enrollable');
    }

    public function series()
    {
        return $this->morphedByMany('CrowAndRaven\CMS\Models\Series', 'enrollable');
    }

    public function enrollUser($id, $type)
    {
        
    }
}
