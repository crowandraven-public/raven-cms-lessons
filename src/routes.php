<?php
Route::group(['prefix' => 'admin', 'namespace' => 'CrowAndRaven\CMS\Controllers\Admin', 'middleware' => ['web', 'content']], function () {
    Route::resource('lessons', 'LessonsController');
    Route::resource('series', 'SeriesController');
    Route::resource('quizzes', 'QuizzesController');
});

Route::group(['middleware' => ['web']], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::post('lesson/like/{id}', ['as' => 'lesson.like', 'uses' => 'CrowAndRaven\CMS\Controllers\LikeLessonsController@likeLesson']);
        Route::post('series/like/{id}', ['as' => 'series.like', 'uses' => 'CrowAndRaven\CMS\Controllers\LikeLessonsController@likeSeries']);

        Route::post('lesson/complete/{id}', ['as' => 'lesson.complete', 'uses' => 'CrowAndRaven\CMS\Controllers\CompleteController@completeLesson']);
        Route::post('series/complete/{id}', ['as' => 'series.complete', 'uses' => 'CrowAndRaven\CMS\Controllers\CompleteController@completeSeries']);
        Route::post('series/{id}/copy', 'CrowAndRaven\CMS\Controllers\Admin\SeriesController@copy'); // clone series
        Route::post('quiz/complete/{id}', ['as' => 'quiz.complete', 'uses' => 'CrowAndRaven\CMS\Controllers\CompleteController@completeQuiz']);
        Route::post('series/quiz/grade/{id}', ['as' => 'quiz.grade', 'uses' => 'CrowAndRaven\CMS\Controllers\SeriesController@gradeQuiz']);

        /* switch to post? */
        Route::get('series/enroll/{id}', ['as' => 'series.enroll', 'uses' => 'CrowAndRaven\CMS\Controllers\EnroillController@enrollSeries']);
    });
});
