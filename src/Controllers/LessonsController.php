<?php

namespace CrowAndRaven\CMS\Controllers;

use Auth;
use Config;
use CrowAndRaven\CMS\Models\Lesson;
use CrowAndRaven\CMS\Models\Tag;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Mail;

class LessonsController extends Controller
{
    /**
     * Show a lesson detail
     * @param  Request $request
     * @param  string  $slug    slug of the lesson
     */
    public function show(Request $request, $slug)
    {
        $lesson = Lesson::getLessonBySlug($slug);
        $this->view_data['lesson'] = $lesson;
        $this->view_data['author'] = User::where('id', $lesson->author)->first();
        $this->view_data['request'] = $request;

        return view('raven::lessons.show', $this->view_data);
    }

    /**
     * Ask the Coach form
     * @param  Request $request form data to send email
     */
    public function contact(Request $request)
    {
        $user = Auth::user();
        
        if (!$user) {
            $data['from_email'] = $request->email;
            $data['from_name'] = $request->name;
        } else {
            $data['from_email'] = $user->email;
            $data['from_name'] = $user->name;
        }

        $data['email_subject'] = 'EVENPULSE Lesson Question';
        $data['content'] = $request->content;
        $data['lesson'] = $request->lesson_title;
        $data['to_email'] = $request->author_email;
        $data['to_name'] = $request->author_name;

        Mail::send('raven::lessons.emails.contact', ['data' => $data], function ($message) use ($data) {
            //$message->from($data['from_email'], $data['from_name']);
            $message->from(Config::get('site.contact.email'), Config::get('site.contact.name'));
            $message->to($data['to_email'], $data['to_name']);
            $message->replyTo($data['from_email'], $data['from_name']);
            $message->subject($data['email_subject']);
        });

        $request->session()->flash('flash_type', 'info');
        $request->session()->flash('flash_title', 'Message Sent');
        $request->session()->flash('flash_message', 'Your message has been successfully sent.');

        return Redirect::back();
    }
}
