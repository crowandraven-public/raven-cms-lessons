<?php

namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use CrowAndRaven\CMS\Models\Quiz;
use CrowAndRaven\CMS\Models\Tag;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class QuizzesController extends Controller
{
    public function __construct()
    {
        $this->middleware('content');
    }

    /**
     * Display a listing of all quizzes.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request->search) && (trim($request->search) != '')) {
            $this->view_data['quizzes'] = Quiz::whereRaw("LOWER(title) LIKE '%".strtolower($request->search)."%'")
                ->orderBy('updated_at', 'desc')->paginate(20);
            $this->view_data['title'] = __('raven::messages.quizzes.index.search.title', ['search' => $request->search]);
        } else {
            $this->view_data['quizzes'] = Quiz::orderBy('updated_at', 'desc')->paginate(20);
        }
        
        $this->view_data['request'] = $request;

        return view('raven::admin.quizzes.index', $this->view_data);
    }

    /**
     * Show the form for creating a new quiz.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->view_data['request'] = $request;
        $this->view_data['user'] = Auth::user();
        $this->view_data['all_topics'] = Tag::tagPairsByType('topic');
        $this->view_data['all_tracks'] = Tag::tagPairsByType('track');
        
        return view('raven::admin.quizzes.create', $this->view_data);
    }

    /**
     * Store a newly created quiz in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $quiz_saved = Quiz::create($request->all());

        // clean and add tags
        $tags = array_merge((array) $request['topic_tags'], (array) $request['track_tags']);
        $tags = array_filter($tags);
        $quiz_saved->tags()->attach($tags);

        if ($quiz_saved) {
            // clean and add tags
            $tags = array_merge((array) $request['topic_tags'], (array) $request['track_tags']);
            $tags = array_filter($tags);
            $quiz_saved->tags()->attach($tags);

            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.quizzes.flash.create.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.quizzes.flash.create.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.quizzes.flash.create.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.quizzes.flash.create.error.message'));
        }

        return Redirect::to('/admin/quizzes');
    }

    /**
     * Show the form for editing the specified quiz.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $quiz = Quiz::findOrFail($id);
        $this->view_data['quiz'] = $quiz;
        $this->view_data['user'] = Auth::user();
        $this->view_data['all_topics'] = Tag::tagPairsByType('topic');
        $this->view_data['quiz_tags'] = $quiz->tags->pluck('id');
        $this->view_data['all_tracks'] = Tag::tagPairsByType('track');
        $this->view_data['request'] = $request;

        return view('raven::admin.quizzes.edit', $this->view_data);
    }

    /**
     * Update the specified quiz in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $quiz_updated = Quiz::updateQuiz($request->all(), $id);

        // sync tags
        $tags = array_merge((array) $request['topic_tags'], (array) $request['track_tags']);
        $tags = array_filter($tags);
        $quiz_updated->tags()->sync($tags);

        if ($quiz_updated) {
            // sync tags
            $tags = array_merge((array) $request['topic_tags'], (array) $request['track_tags']);
            $tags = array_filter($tags);
            $quiz_updated->tags()->sync($tags);

            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.quizzes.flash.edit.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.quizzes.flash.edit.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.quizzes.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.quizzes.flash.edit.error.message'));
        }

        return Redirect::to('/admin/quizzes');
    }

    /**
     * Remove the specified quiz from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $quiz = Quiz::findOrFail($id);

        if ($quiz->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.quizzes.flash.delete.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.quizzes.flash.delete.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.quizzes.flash.delete.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.quizzes.flash.delete.error.message'));
        }

        return Redirect::to('/admin/quizzes');
    }
}
