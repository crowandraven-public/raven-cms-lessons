<?php

namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use CrowAndRaven\CMS\Models\Media;
use CrowAndRaven\CMS\Models\Lesson;
use CrowAndRaven\CMS\Models\Quiz;
use CrowAndRaven\CMS\Models\Resource;
use CrowAndRaven\CMS\Models\Tag;
use CrowAndRaven\CMS\Models\User;
use CrowAndRaven\CMS\Models\Attachment;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;

class LessonsController extends Controller
{
    public function __construct()
    {
        $this->middleware('content');
    }

    /**
     * Display a listing of all lessons.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request->search) && (trim($request->search) != '')) {
            $this->view_data['lessons'] = Lesson::whereRaw("LOWER(title) LIKE '%".strtolower($request->search)."%'")
                ->orderBy('updated_at', 'desc')->paginate(20);
            $this->view_data['title'] = __('raven::messages.lessons.index.search.title', ['search' => $request->search]);
        } else {
            $this->view_data['lessons'] = Lesson::orderBy('updated_at', 'desc')->paginate(20);
        }
        
        $this->view_data['request'] = $request;

        return view('raven::admin.lessons.index', $this->view_data);
    }

    /**
     * Show the form for creating a new lesson.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->view_data['request'] = $request;
        $this->view_data['user'] = Auth::user();
        $this->view_data['all_topics'] = Tag::tagPairsByType('topic');
        $this->view_data['all_tracks'] = Tag::tagPairsByType('track');
        
        $this->view_data['using_attachments'] = $using_attachments = config('raven-cms.show.lessons.attachments');

        if ($using_attachments) {
            $this->view_data['all_images'] = Attachment::getAllAttachmentsByType('image')->prepend('Select One', 0);
            $this->view_data['all_files'] = Attachment::getAllAttachmentsByType('document')->prepend('Select One', 0);
        } else {
            $this->view_data['all_images'] = Media::getAllImages()->prepend('Select One', 0);
            $this->view_data['all_files'] = Media::getAllFiles()->prepend('Select One', 0);
        }

        $this->view_data['all_authors'] = User::getUsersByRole('author')->prepend('Select One', 0);
        $this->view_data['all_resources'] = Resource::getAllResources()->prepend('Select One', 0);
        $this->view_data['all_quizzes'] = Quiz::getAllQuizzes()->prepend('Select One', 0);
        
        return view('raven::admin.lessons.create', $this->view_data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lesson_saved = Lesson::create($request->all());

        if ($lesson_saved) {
            // clean and add tags
            $tags = array_merge((array) $request['topic_tags'], (array) $request['track_tags']);
            $tags = array_filter($tags);
            $lesson_saved->tags()->attach($tags);

            // clean and add resources
            $resources = [];
            if ($request['resources']) {
                $resources = array_filter($request['resources']);
                $lesson_saved->resources()->attach($resources);
            }

            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.lessons.flash.create.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.lessons.flash.create.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.lessons.flash.create.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.lessons.flash.create.error.message'));
        }

        return Redirect::to('/admin/lessons');
    }

    /**
     * Show the form for editing the specified lesson.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $lesson = Lesson::findOrFail($id);
        $this->view_data['lesson'] = $lesson;
        $this->view_data['user'] = Auth::user();
        $this->view_data['all_topics'] = Tag::tagPairsByType('topic');
        $this->view_data['lesson_tags'] = $lesson->tags->pluck('id');
        $this->view_data['all_tracks'] = Tag::tagPairsByType('track');
        $this->view_data['using_attachments'] = $using_attachments = config('raven-cms.show.lessons.attachments');

        if ($using_attachments) {
            $this->view_data['all_images'] = Attachment::getAllAttachmentsByType('image')->prepend('Select One', 0);
            $this->view_data['all_files'] = Attachment::getAllAttachmentsByType('document')->prepend('Select One', 0);
        } else {
            $this->view_data['all_images'] = Media::getAllImages()->prepend('Select One', 0);
            $this->view_data['all_files'] = Media::getAllFiles()->prepend('Select One', 0);
        }
        
        $this->view_data['all_authors'] = User::getUsersByRole('author')->prepend('Select One', 0);
        $this->view_data['resources'] = $lesson->resources->pluck('id');
        $this->view_data['all_resources'] = Resource::getAllResources()->prepend('Select One', 0);
        $this->view_data['all_quizzes'] = Quiz::getAllQuizzes()->prepend('Select One', 0);
        $this->view_data['request'] = $request;

        return view('raven::admin.lessons.edit', $this->view_data);
    }

    /**
     * Update the specified lesson in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lesson_updated = Lesson::updateLesson($request->all(), $id);

        if ($lesson_updated) {
            // sync tags
            $tags = array_merge((array) $request['topic_tags'], (array) $request['track_tags']);
            $tags = array_filter($tags);
            $lesson_updated->tags()->sync($tags);

            // sync resources
            $resources = [];
            if ($request['resources']) {
                $resources = array_filter($request['resources']);
                $lesson_updated->resources()->sync($resources);
            }

            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.lessons.flash.edit.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.lessons.flash.edit.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.lessons.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.lessons.flash.edit.error.message'));
        }

        return Redirect::to('/admin/lessons');
    }

    /**
     * Remove the specified lesson from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $lesson = Lesson::findOrFail($id);

        if ($lesson->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.lessons.flash.delete.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.lessons.flash.delete.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.lessons.flash.delete.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.lessons.flash.delete.error.message'));
        }

        return Redirect::to('/admin/lessons');
    }
}
