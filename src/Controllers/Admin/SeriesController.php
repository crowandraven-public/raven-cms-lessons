<?php

namespace CrowAndRaven\CMS\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use CrowAndRaven\CMS\Models\Lesson;
use CrowAndRaven\CMS\Models\Media;
use CrowAndRaven\CMS\Models\Series;
use CrowAndRaven\CMS\Models\Tag;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use CrowAndRaven\CMS\Models\Attachment;

class SeriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('content');
    }

    /**
     * Display a listing of all series.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request->search) && (trim($request->search) != '')) {
            $this->view_data['series_array'] = Series::whereRaw("LOWER(title) LIKE '%".strtolower($request->search)."%'")
                ->orderBy('updated_at', 'desc')->paginate(20);
            $this->view_data['title'] = __('raven::messages.series.index.search.title', ['search' => $request->search]);
        } else {
            $this->view_data['series_array'] = Series::orderBy('updated_at', 'desc')->paginate(20);
        }
        
        $this->view_data['request'] = $request;

        return view('raven::admin.series.index', $this->view_data);
    }

    /**
     * Show the form for creating a new series.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->view_data['request'] = $request;
        $this->view_data['user'] = Auth::user();
        $this->view_data['all_topics'] = Tag::tagPairsByType('topic');
        $this->view_data['all_tracks'] = Tag::tagPairsByType('track');
        $this->view_data['all_images'] = Media::getAllImages()->prepend('Select One', 0);

        $this->view_data['using_attachments'] = $using_attachments = config('raven-cms.show.series.attachments');

        if ($using_attachments) {
            $this->view_data['all_images'] = Attachment::getAllAttachmentsByType('image')->prepend('Select One', 0);
        } else {
            $this->view_data['all_images'] = Media::getAllImages()->prepend('Select One', 0);
        }


        $this->view_data['all_authors'] = User::getUsersByRole('author')->prepend('Select One', 0);
        $this->view_data['all_lessons'] = Lesson::getAllLessons()->prepend('Select One', 0);
        
        return view('raven::admin.series.create', $this->view_data);
    }

    /**
     * Store a newly created series in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $series_saved = Series::create($request->all());

        if ($series_saved) {
            // add tags
            $tags = [];
            $tags = array_merge((array) $request['topic_tags'], (array) $request['track_tags']);
            $tags = array_filter($tags);
            $series_saved->tags()->attach($tags);

            // add lessons
            $lessons = [];
            if ($request['lessons']) {
                $lessons = array_filter($request['lessons']);
                foreach ($lessons as $key => $lesson) {
                    $series_saved->lessons()->attach($lesson, ['order' => $key + 1]);
                }
            }

            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.series.flash.create.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.series.flash.create.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.series.flash.create.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.series.flash.create.error.message'));
        }

        return Redirect::to('/admin/series');
    }

    /**
     * Show the form for editing the specified series.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $series = Series::findOrFail($id);
        $this->view_data['series'] = $series;
        $this->view_data['user'] = Auth::user();
        $this->view_data['all_topics'] = Tag::tagPairsByType('topic');
        $this->view_data['series_tags'] = $series->tags->pluck('id');
        $this->view_data['all_tracks'] = Tag::tagPairsByType('track');
        
        $this->view_data['using_attachments'] = $using_attachments = config('raven-cms.show.series.attachments');

        if ($using_attachments) {
            $this->view_data['all_images'] = Attachment::getAllAttachmentsByType('image')->prepend('Select One', 0);
        } else {
            $this->view_data['all_images'] = Media::getAllImages()->prepend('Select One', 0);
        }

        $this->view_data['all_authors'] = User::getUsersByRole('author')->prepend('Select One', 0);
        $this->view_data['all_lessons'] = Lesson::getAllLessons()->prepend('Select One', 0);
        $this->view_data['lessons'] = $series->lessons()->orderBy('order')->pluck('id');
        $this->view_data['request'] = $request;

        return view('raven::admin.series.edit', $this->view_data);
    }

    /**
     * Update the specified series in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $series_updated = Series::updateSeries($request->all(), $id);

        if ($series_updated) {
            // sync tags
            $tags = [];
            $tags = array_merge((array) $request['topic_tags'], (array) $request['track_tags']);
            $tags = array_filter($tags);
            $series_updated->tags()->sync($tags);

            // sync lessons
            $lessons = [];
            $lessons_array = [];
            if ($request['lessons']) {
                $lessons = array_filter($request['lessons']);
                foreach ($lessons as $key => $id) {
                    $lessons_array[$id] = ['order' => $key + 1];
                }
                //dd($lessons_array);
                $series_updated->lessons()->sync($lessons_array);
            }

            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.series.flash.edit.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.series.flash.edit.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.series.flash.edit.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.series.flash.edit.error.message'));
        }

        return Redirect::to('/admin/series');
    }

    /**
     * Remove the specified series from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->validate($request, [
            'confirm_destroy' => 'required|in:DESTROY',
        ]);

        $series = Series::findOrFail($id);

        if ($series->delete()) {
            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.series.flash.delete.success.title'));
            $request->session()->flash('flash_message', __('raven::messages.series.flash.delete.success.message'));
        } else {
            $request->session()->flash('flash_type', 'warning');
            $request->session()->flash('flash_title', __('raven::messages.series.flash.delete.error.title'));
            $request->session()->flash('flash_message', __('raven::messages.series.flash.delete.error.message'));
        }

        return Redirect::to('/admin/series');
    }

    /**
     * Duplicate a series
     * @param  Request $request
     */
    public function copy(Request $request, $id)
    {
        $existing_series = Series::findOrFail($id);
        $series_copy = $existing_series->replicate();
        
        $title_json = json_decode($series_copy->title);
        $title_json->en = $title_json->en.' (copy)';
        $series_copy->title = json_encode($title_json);

        $slug_json = json_decode($series_copy->slug);
        $slug_json->en = $slug_json->en.'-copy';
        $series_copy->slug = json_encode($slug_json);
        $series_copy->save();

        // copy lessons
        $existing_lessons = $existing_series->lessons->pluck('id', 'pivot.order');
        $lessons_array = [];
        foreach ($existing_lessons  as $order => $id) {
            $lessons_array[$id] = ['order' => $order + 1];
        }
        $series_copy->lessons()->sync($lessons_array);


        // copy tags
        //$series_saved->tags()->attach($tags);
        $existing_series_tags = $existing_series->tags()->get();
        $tags_to_attach = [];
        foreach($existing_series_tags as $tag) {
            $tags_to_attach[] = $tag->id;
        }
       
        $series_copy->tags()->attach($tags_to_attach);

        if ($series_copy->save()) {
            $series_copy->lessons = $existing_series->lessons;
            $series_copy->tags()->sync($existing_series->tags);

            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', __('raven::messages.series.flash.copy.title'));
            $request->session()->flash('flash_message', __('raven::messages.series.flash.copy.message'));
        }
        

        return Redirect::back();

    }
}
