<?php

namespace CrowAndRaven\CMS\Controllers;

use Auth;
use CrowAndRaven\CMS\Models\Lesson;
use CrowAndRaven\CMS\Models\Series;
use CrowAndRaven\CMS\Models\Post;
use CrowAndRaven\CMS\Models\Page;
use CrowAndRaven\CMS\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class CatalogController extends Controller
{
    /**
     * Show catalog default page -- currently not used on this site.
     * @param  Request $request
     */
    public function index(Request $request)
    {
        $page = Page::getPageBySlug('catalog');
        $this->view_data['page'] = $page;
        $this->view_data['request'] = $request;

        return view('raven::catalog.index', $this->view_data);
    }

    /**
     * Show all lessons in the catalog
     * @param  Request $request
     */
    public function lessons(Request $request)
    {
        $page = Page::getPageBySlug('lessons');
        $this->view_data['page'] = $page;
        
        // get all lessons
        $track = (Auth::user()) ? Auth::user()->track : null;
        $this->view_data['lessons'] = Lesson::getLessons(['basic'], $track)->paginate(15);
        $this->view_data['request'] = $request;

        return view('raven::catalog.lessons', $this->view_data);
    }

    /**
     * Show all series in the catalog
     * @param  Request $request
     */
    public function series(Request $request)
    {
        $page = Page::getPageBySlug('series');
        $this->view_data['page'] = $page;
        
        // get user info
        $user = Auth::user();
        if (isset($user->currentTeam)) {
            $team = ($user->currentTeam->id) ? $user->currentTeam->id : null;
        }
        $track = ($user) ? $user->track : null;

        // get all basic series
        if (isset($team)) {
            $this->view_data['seriess'] = Series::getSeries(['basic'], $track)->custom($user->currentTeam->id)->paginate(15);
        } else {
            $this->view_data['seriess'] = Series::getSeries(['basic'], $track)->public()->paginate(15);
        }

        $this->view_data['request'] = $request;

        return view('raven::catalog.series', $this->view_data);
    }

    /**
     * Show all courses in the catalog
     * @param  Request $request
     */
    public function courses(Request $request)
    {
        $page = Page::getPageBySlug('courses');
        $this->view_data['page'] = $page;
        
        // get user info
        $user = Auth::user();
        if (isset($user->currentTeam)) {
            $team = ($user->currentTeam->id) ? $user->currentTeam->id : null;
        }
        $track = ($user) ? $user->track : null;

        // get all premuim series
        if (isset($team)) {
            $this->view_data['seriess'] = Series::getSeries(['premium'], $track)->custom($user->currentTeam->id)->paginate(15);
        } else {
            $this->view_data['seriess'] = Series::getSeries(['premium'], $track)->public()->paginate(15);
        }

        $this->view_data['request'] = $request;

        return view('raven::catalog.series', $this->view_data);
    }

    /**
     * Show all webinars in the catalog
     * @param  Request $request
     */
    public function webinars(Request $request)
    {
        $page = Page::getPageBySlug('webinars');
        $this->view_data['page'] = $page;

        // get all webinars
        $track = (Auth::user()) ? Auth::user()->track : null;
        $this->view_data['posts'] = Post::getPosts('webinars', $track)->paginate(15);
        $this->view_data['request'] = $request;

        return view('raven::catalog.webinars', $this->view_data);
    }

    /**
     * Show a users saved and completed lessons, courses, etc
     * @param  Request $request
     */
    public function mine(Request $request)
    {
        $page = Page::getPageBySlug('my-evenpulse');
        $this->view_data['page'] = $page;
        
        return view('raven::catalog.my', $this->view_data);
    }
}
