<?php

namespace CrowAndRaven\CMS\Controllers;

use CrowAndRaven\CMS\Controllers\LikeController as LikeControllerCore;
use CrowAndRaven\CMS\Models\Complete;
use CrowAndRaven\CMS\Models\Lesson;
use CrowAndRaven\CMS\Models\Quiz;
use CrowAndRaven\CMS\Models\Series;
use Illuminate\Support\Facades\Auth;

class CompleteController extends LikeControllerCore
{
    public function completeLesson($id)
    {
        $exists = Lesson::find($id)->exists();

        if ($exists) {
            return $this->handleComplete('lessons', $id);
        }
        $this->getJsonFail();
    }

    public function completeSeries($id)
    {
        $exists = Series::find($id)->exists();

        if ($exists) {
            return $this->handleComplete('series', $id);
        }
        $this->getJsonFail();
    }

    public function completeQuiz($id)
    {
        $exists = Quiz::find($id)->exists();

        if ($exists) {
            return $this->handleComplete('quiz', $id);
        }
        $this->getJsonFail();
    }

    public function handleComplete($type, $id, $return_json=true)
    {
        $existing_complete = Complete::withTrashed()->whereCompleteableType($type)->whereCompleteableId($id)->whereUserId(Auth::id())->first();
        $liked = false;
        
        if (is_null($existing_complete)) {
            Complete::create([
                'user_id'       => Auth::id(),
                'completeable_id'   => $id,
                'completeable_type' => $type,
            ]);
            $liked = true;
        } else {
            if (is_null($existing_complete->deleted_at)) {
                $existing_complete->delete();
                $liked = false;
            } else {
                $existing_complete->restore();
                $liked = true;
            }
        }
        if ($return_json == true) {
            return response()->json([
                'success' => true,
                'liked' => $liked,
                'post_id' => $id,
                'type' => $type
            ]);
        }

        return $liked;
    }

    public function getJsonFail()
    {
        return response()->json([
            'success' => false
        ]);
    }
}
