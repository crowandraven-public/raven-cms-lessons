<?php

namespace CrowAndRaven\CMS\Controllers;

use Auth;
use Config;
use CrowAndRaven\CMS\Models\Lesson;
use CrowAndRaven\CMS\Models\Quiz;
use CrowAndRaven\CMS\Models\Series;
use CrowAndRaven\CMS\Models\User;
use CrowAndRaven\CMS\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Mail;
use PDF;

class SeriesController extends Controller
{
    /**
     * Show series detail view
     * @param  string $slug unique slug of series
     */
    public function show(Request $request, $slug)
    {
        $series = Series::getSeriesBySlug($slug);
        $this->view_data['series'] = $series;
        $this->view_data['request'] = $request;
        //$this->view_data['price'] = json_decode($series->config)->price;

        // get lessons
        $lessons = $series->lessons()->orderBy('order')->get();
        $this->view_data['lessons'] = $lessons;
        $lesson_count = is_var_iterable($lessons) ? count($lessons) : 0;
        $this->view_data['lesson_count'] = $lesson_count;
        $this->view_data['length'] = round($lessons->pluck('length')->sum());

        if (Auth::user()) {
            $user = Auth::user();
            
            // completed?
            $user_completed_lessons = Auth::user()->completedLessons->whereIn('pivot.completeable_id', $lessons->pluck('id'))->pluck('pivot.completeable_id')->toArray();
            
            $this->view_data['user_completed_lessons'] = $user_completed_lessons;
            $user_completed_lessons_count = is_var_iterable($user_completed_lessons) ? count($user_completed_lessons) : 0;
            
            // get percent complete
            $this->view_data['percent_complete'] = ($lesson_count > 0) ? ($user_completed_lessons_count/$lesson_count)*100 : 0;
        }

        // get coach/author
        $this->view_data['author'] = User::where('id', $series->author)->first();

        return view('raven::series.show', $this->view_data);
    }

    /**
     * Show a lesson detail from a specific series
     * @param  Request $request
     * @param  string  $series_slug the slug of the series
     * @param  string  $lesson_slug the slug of the lesson
     */
    public function showLesson(Request $request, $series_slug, $lesson_slug)
    {
        // get auth user
        $user = Auth::user();
        $this->view_data['user'] = $user;

        // get series
        $series = Series::getSeriesBySlug($series_slug);

        // get lesson
        $lesson = Lesson::getLessonBySlug($lesson_slug);
        
        // get lesson with pivot fields
        $series_lesson = $series->lessons->where('id', $lesson->id)->first();

        $this->view_data['lesson'] = $lesson;
        $this->view_data['author'] = User::where('id', $lesson->author)->first();
        $this->view_data['request'] = $request;

        // lesson bookmarked/liked by user | is the lesson completed
        $this->view_data['user_liked'] = false;
        $this->view_data['user_completed'] = false;
        if ($user) {
            // liked?
            
            $user_likes = $user->likedLessons->pluck('likeable_id')->toArray();
            if (in_array($lesson->id, $user_likes)) {
                $this->view_data['user_liked'] = true;
            }
            // completed?
            $user_completed_items = $user->completedLessons->pluck('id')->toArray();

            if (in_array($lesson->id, $user_completed_items)) {
                $this->view_data['user_completed'] = true;
            }

            // liked and completed for the related section
            $this->view_data['user_likes'] = [];
            $this->view_data['user_completed_items'] = [];
            $this->view_data['user_likes'] = $user_likes;
            $this->view_data['user_completed_items'] = $user_completed_items;
        }
        
        
        $this->view_data['series'] = $series;

        $lessons = $series->lessons->sortBy('order');
        $this->view_data['lessons'] = $lessons;
        
        //get quiz
        if ($lesson->quiz) {
            $quiz = Quiz::where('id', $lesson->quiz)->firstOrFail();
            $this->view_data['quiz_questions'] = json_decode($quiz->config);
            $this->view_data['quiz'] = $quiz;
        }
        
        // get next lesson
        $this->view_data['next'] = Lesson::getNextLesson($series->id, $series_lesson->pivot->order)->first();
        // is the lesson completed
        
        if ($user) {
            // completed?
            $user_completed_lessons = Auth::user()->completedLessons->whereIn('pivot.completeable_id', $lessons->pluck('id'))->pluck('pivot.completeable_id')->toArray();
            $user_completed_lessons_count = is_var_iterable($user_completed_lessons) ? count($user_completed_lessons) : 0;
        
            $user_completed_items = $user->completedLessons->pluck('completeable_id')->toArray();
            
            if (in_array($lesson->id, $user_completed_items)) {
                $this->view_data['user_completed'] = true;
            }
            // liked and completed for the related section
            $this->view_data['user_likes'] = [];
            $this->view_data['user_completed_items'] = [];
            $this->view_data['user_completed_lessons'] = [];
            $this->view_data['user_likes'] = $user->likedLessons->pluck('likeable_id')->toArray();
            $this->view_data['user_completed_items'] = $user->completedLessons->pluck('completeable_id')->toArray();
            $this->view_data['user_completed_lessons'] = $user_completed_lessons;

            $series_lessons_count = $series->lessons->count();

            // get percent complete
            $this->view_data['percent_complete'] = ($user_completed_lessons_count/$series_lessons_count)*100;
            
            // check if user has completed all lessons
            $this->view_data['is_series_completed'] = Series::isCompleted($series_lessons_count, $user_completed_lessons_count);
        }
        
        // get coach/author
        $this->view_data['author'] = User::where('id', $series->author)->first();

        return view('raven::series.lesson', $this->view_data);
    }

    /**
     * Ask the Coach form
     * @param  Request $request form data to send email
     */
    public function contact(Request $request)
    {
        $user = Auth::user();

        if (!$user) {
            $data['from_email'] = $request->email;
            $data['from_name'] = $request->name;
        } else {
            $data['from_email'] = $user->email;
            $data['from_name'] = $user->name;
        }

        $data['email_subject'] = 'EVENPULSE Course Question';
        $data['content'] = $request->content;
        $data['course'] = $request->course_title;
        $data['to_email'] = $request->author_email;
        $data['to_name'] = $request->author_name;

        Mail::send('raven::series.emails.contact', ['data' => $data], function ($message) use ($data) {
            //$message->from($data['from_email'], $data['from_name']);
            $message->from(Config::get('site.contact.email'), Config::get('site.contact.name'));
            $message->to($data['to_email'], $data['to_name']);
            $message->replyTo($data['from_email'], $data['from_name']);
            $message->subject($data['email_subject']);
        });

        $request->session()->flash('flash_type', 'info');
        $request->session()->flash('flash_title', 'Message Sent');
        $request->session()->flash('flash_message', 'Your message has been successfully sent.');

        return Redirect::back();
    }

    public function gradeQuiz(Request $request, $id)
    {
        $user = Auth::user();

        // get quiz
        $quiz = Quiz::where('id', $id)->firstOrFail();

        // get lesson
        $lesson = Lesson::find($request->lesson);;
        if (!$lesson) {
            return Redirect::back();
        }
        // get series
        $series = Series::find($request->series);

        if (!$series) {
            return Redirect::back();
        }
        
        $questions_answered_correctly = 0;
        $quiz_config = json_decode($quiz->config);
        $number_needed_to_pass = count($quiz_config->questions);
        foreach( $quiz_config->questions as $qindex => $question) {
            $answer_input = $request->input('answer_'.($qindex + 1));
            if(!isset($answer_input)) {
                continue;
            }

            foreach($question->answers as $answer_index => $answer) {
                if($answer_input == $answer->itemValue && $answer->itemCheckboxState == 1) {
                    $questions_answered_correctly+=1;
                }
            }
        }
        
        if ($questions_answered_correctly === $number_needed_to_pass) {
            // mark lesson as completed
            $completed = $this->setComplete($request->lesson);
            // get URL of next lesson
            if (isset($request->next) && $request->next != '') {
                $redirect_path = '/series/'.localize($series->slug).'/'.localize($request->next);
            } else {
                $redirect_path = '/series/'.localize($series->slug);
            }

            $request->session()->flash('flash_type', 'success');
            $request->session()->flash('flash_title', 'Quiz Passed');
            $request->session()->flash('flash_message', 'Awesome. You scored 100% on the quiz. Now you can continue to the next lesson or complete the series.');
        } else {
            $request->session()->flash('flash_type', 'danger');
            $request->session()->flash('flash_title', 'Quiz Failed');
            $request->session()->flash('flash_message', 'Hmmm. You didn\'t score 100% on the quiz. Please try again.');

            return Redirect::back();
        }

        return Redirect::to($redirect_path);
    }

    /**
     * Show the certificate if user completed the course
     * @param  Request $request [description]
     * @param  [type]  $id      [description]
     * @return [type]           [description]
     */
    public function getCertificate(Request $request, $id)
    {
        $user = Auth::user();
        if (!$user) {
            return Redirect::back();
        }

        // get series
        $series = Series::where('id', $id)->first();
        $this->view_data['series'] = $series;

        // get series lessons
        $lessons = $series->lessons;
        $lessons_count = is_var_iterable($lessons) ? count($lessons) : 0;
        $this->view_data['lessons'] = $lessons;
        $user_completed_lessons = Auth::user()->completedLessons->whereIn('pivot.completeable_id', $lessons->pluck('id'))->pluck('pivot.completeable_id')->toArray();
        $user_completed_lessons_count = is_var_iterable($user_completed_lessons) ? count($user_completed_lessons) : 0;
        
        // check if user has completed all lessons
        $is_series_completed = Series::isCompleted($user_completed_lessons, $user_completed_lessons_count);

        // INCLUDE THE phpToPDF.php FILE
        require(__DIR__.'/../../resources/views/series/pdf/phpToPDF.php');

        // PUT YOUR HTML IN A VARIABLE
        $my_html="<HTML>
        <body style=\"margin:0; padding:0; height: 100%; width:auto; background-image: url('https://evenpulse.com/storage/uploads/d762b1f0b.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center; \">
            <div style=\"display:block; padding:0; text-align: center; padding-top: 270px;\">
                <p>This certifies that:</p>
                <h2>".$user->name."</h2>
                <p>has successfully completed the training series of</p>
                <h2>".localize($series->title)."</h2>
                on ".date('M d, Y')."
            </div>
        </body>
        </HTML>";

        // SET YOUR PDF OPTIONS -- FOR ALL AVAILABLE OPTIONS, VISIT HERE:  http://phptopdf.com/documentation/
        $pdf_options = array(
          "source_type" => 'html',
          "header" => '',
          "footer" => '',
          "margin"=>array("right"=>"0","left"=>'0',"top"=>"0","bottom"=>"0"),
          "source" => $my_html,
          "action" => 'view', // change to download
          "color" => 'color',
          "page_orientation" => 'landscape');

        // CALL THE phpToPDF FUNCTION WITH THE OPTIONS SET ABOVE
        phptopdf($pdf_options);
       
        if ($is_series_completed) {
            dd('create certificate');
        } else {
            dd('series not complete');
        }

        $this->view_data['request'] = $request;

        return view('raven::series.certificate', $this->view_data);
    }


    public function setComplete($id)
    {
        $user = Auth::user();

        if (!$user) {
            return Redirect::back();
        }

        $lesson = Lesson::findOrFail($id);
        if (!$lesson) {
            return Redirect::back();
        }

        $complete_controller = new \CrowAndRaven\CMS\Controllers\CompleteController;

        return $complete_controller->completeLesson($id);
    }

    public function setIncomplete($id)
    {
        $user = Auth::user();
        
        if (!$user) {
            return Redirect::back();
        }

        $lesson = Lesson::findOrFail($id);
        if (!$lesson) {
            return Redirect::back();
        }

        // save lesson
        if ($lesson->setIncomplete($user->id, $id)) {
            return response()->json([
                'success' => true,
                'completeableAction' => 'setComplete'
            ]);
        }
        return response()->json([
            'success' => false
        ]);
    }
}
