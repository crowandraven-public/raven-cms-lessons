<?php

namespace CrowAndRaven\CMS\Controllers;

use CrowAndRaven\CMS\Models\Enroll;
use CrowAndRaven\CMS\Models\Series;
use Illuminate\Support\Facades\Auth;

class EnrollController extends Controller
{
    public function enrollSeries($id)
    {
        $exists = Series::find($id)->exists();

        if ($exists) {
            $this->handleEnroll('series', $id);
        }
        
        return redirect()->back();
    }

    public function handleEnroll($type, $id)
    {
        $existing_enroll = Enroll::withTrashed()->whereEnrollableType($type)->whereEnrollableId($id)->whereUserId(Auth::id())->first();

        if (is_null($existing_enroll)) {
            Complete::create([
                'user_id'         => Auth::id(),
                'enrollable_id'   => $id,
                'enrollable_type' => $type,
            ]);
        } else {
            if (is_null($existing_enroll->deleted_at)) {
                $existing_enroll->delete();
            } else {
                $existing_enroll->restore();
            }
        }
    }
}
