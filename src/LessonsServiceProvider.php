<?php

namespace CrowAndRaven\CMS;

use Collective\Html\HtmlServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
                                                                                                            
/************************************ version 0.1 ***************************************
                                                                              
                                                                                                                    
                      ......
                  'coxO0000Odoc'.                .ool.            .;cloo'.
                ;xXKko:,,'.;lxKX0ko:,.           'OXXk       ,ok0Oolcd00xdl;..
              .:dOx       .oKXNKOKXKxl;        .kNNNXO,   'cxKNKd,.   ..dkO0xl;
              .;oo;.''',;ckXNNk' .:oOK0x:'.     lNNNNNd':kXNKxc'         .;ldOOx
       ...,lo;,::;lc:;;lONNN0:.    'cdNNXkdol;' XNNNNOk 0d;..              .cOKKd:
    ok0O00oco,..    .lXNNx.         .,cKNNXXOx WNNNO Xkoodxd;              .ck0KK0
   0Oxl;':dlxl.    .oXXo.         .....kXXX0dOWN KxO c;,;d0k:.             .;xXKkood
  kl:'.  .lxOO     'xXO'        'oxlllldk0XXkd0XXxdO;     .cxxc'.            .cO  ':lo
 Od.      .ckO      'kXx.       :xc..    'dKXOxkkOK        'lOOdl        .lOk  ;,..
xc        .;l:     .,cl'     .::.        .;xKXXXXK          'xK0xxk       'dKO   .'c
,        ..'.       .;o:   .;,.           ,OXXNNd.          .oOc. ..     .d00    .co
.        ,ol        'l:.  .'.             .l0XN0,          .x0:   .l      xd:.   ox.
  .;    d...        .'cdc;oko:.            .:OXKk;          'od'   .O    xdl    Oc
    .,cOo'....     .':oo::ccl,            ,clxxOl'         .,;,.   .,l  ':do'...,. ,Ox
    .:kkc,,.;l      ..,;:c',c:,'.           ;c.:xkc.       .,:;';'''.l  ,,,::;,oOko, .:
   ...lkoox;.coo:l,  ..;ll:cx:....;;        .loxK0xlc   ''. .,l:.'',,;k',,lo:.,::ddcl;..
  'lx00kldl,,:ll:,.,'.odcodc:c;;odc:;,;::,,.'lx0KXX:::'.:llo,.,:,,.':,::;cdxd;l0xdo;','..
;:;:d0XNNXN0KX0xOKk00Okl. .,;cc,';lkklccokd:oxlc;,;,;,:oc,'cdOK0l.,,''.;;,.';:;,.........

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 ____  _   _    _    ____   _____        __  __  __  ___  _   _ ____ _____ _____ ____
/ ___|| | | |  / \  |  _ \ / _ \ \      / / |  \/  |/ _ \| \ | / ___|_   _| ____|  _ \
\___ \| |_| | / _ \ | | | | | | \ \ /\ / /  | |\/| | | | |  \| \___ \ | | |  _| | |_) |
 ___) |  _  |/ ___ \| |_| | |_| |\ V  V /   | |  | | |_| | |\  |___) || | | |___|  _ <
|____/|_| |_/_/   \_\____/ \___/  \_/\_/    |_|  |_|\___/|_| \_|____/ |_| |_____|_| \_\

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx*/

class LessonsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        // register morphMap
        Relation::morphMap([
            'lessons' => 'CrowAndRaven\CMS\Models\Lesson',
            'quizzes' => 'CrowAndRaven\CMS\Models\Quiz',
            'series' => 'CrowAndRaven\CMS\Models\Series'
        ]);

        if ($this->app->runningInConsole()) {
            if (!class_exists('AddTrackToUsersTable')) {
                $timestamp = date('Y_m_d_His', time());
                $this->publishes([__DIR__.'/../database/migrations/add_track_to_users_table.php.stub' => database_path('migrations/'.$timestamp.'_add_track_to_users_table.php')], 'raven-lessons-migrations');
            }

            if (!class_exists('CreateLessonsTable')) {
                $timestamp = date('Y_m_d_His', time() + 10);
                $this->publishes([__DIR__.'/../database/migrations/create_lessons_table.php.stub' => database_path('migrations/'.$timestamp.'_create_lessons_table.php')], 'raven-lessons-migrations');
            }

            if (!class_exists('CreateQuizzesTable')) {
                $timestamp = date('Y_m_d_His', time() + 20);
                $this->publishes([__DIR__.'/../database/migrations/create_quizzes_table.php.stub' => database_path('migrations/'.$timestamp.'_create_quizzes_table.php')], 'raven-lessons-raven-lessons-migrations');
            }

            if (!class_exists('CreateSeriesTable')) {
                $timestamp = date('Y_m_d_His', time() + 30);
                $this->publishes([__DIR__.'/../database/migrations/create_series_table.php.stub' => database_path('migrations/'.$timestamp.'_create_series_table.php')], 'raven-lessons-migrations');
            }

            if (!class_exists('CreateSeriesTeamTable')) {
                $timestamp = date('Y_m_d_His', time() + 35);
                $this->publishes([__DIR__.'/../database/migrations/create_series_team_table.php.stub' => database_path('migrations/'.$timestamp.'_create_series_team_table.php')], 'raven-lessons-migrations');
            }

            if (!class_exists('CreateSeriesablesTable')) {
                $timestamp = date('Y_m_d_His', time() + 40);
                $this->publishes([__DIR__.'/../database/migrations/create_seriesables_table.php.stub' => database_path('migrations/'.$timestamp.'_create_seriesables_table.php'),
                    ], 'raven-lessons-migrations');
            }

            if (!class_exists('CreateCompletablesTable')) {
                $timestamp = date('Y_m_d_His', time() + 50);
                $this->publishes([__DIR__.'/../database/migrations/create_completeables_table.php.stub' => database_path('migrations/'.$timestamp.'_create_completeables_table.php'),
                    ], 'raven-lessons-migrations');
            }

            if (!class_exists('CreateEnrollablesTable')) {
                $timestamp = date('Y_m_d_His', time() + 60);
                $this->publishes([__DIR__.'/../database/migrations/create_enrollables_table.php.stub' => database_path('migrations/'.$timestamp.'_create_enrollables_table.php'),
                    ], 'raven-lessons-migrations');
            }

            $this->commands([
                Console\Commands\InstallLessons::class
            ]);

            $this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/raven'),
            ], 'raven-lessons-lang');

            $this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/raven'),
            ], 'raven-lessons-views');
        }

        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'raven');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(raven::class, function () {
            return new CMS();
        });

        $this->app->alias(raven::class, 'raven');
    }
}
